<?php

namespace App\Model;

use App\Core\Model;

class InformacaoContato extends Model
{

    public function listaTodos()
    {
        $sql = "SELECT * FROM `informacao_contato` WHERE 1";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function atualizar($id, $endereco, $numero, $bairro, $cep, $telefone, $celular, $whatsapp, $email, $facebook, $instagram, $linkedin, $youtube, $twitter, $abertura, $fechamento )
    {
        $sql = "update informacao_contato set endereco = '".$endereco."', numero = '".$numero."',cep = '".$cep."',telefone = '".$telefone."',celular = '".$celular."',whatsapp = '".$whatsapp."',email = '".$email."',facebook = '".$facebook."',instagram = '".$instagram."',linkedin = '".$linkedin."',twitter = '".$twitter."',youtube = '".$youtube."',abertura = '".$abertura."',fechamento = '".$fechamento."' where id = ".$id;
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

}
