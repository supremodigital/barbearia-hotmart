<?php

namespace App\Model;

use App\Core\Model;

class Reservatempo extends Model
{

    public function listaTodos()
    {
        $sql = "SELECT * FROM `reserva_tempo` WHERE 1 ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function reservaHoje($data)
    {
        $sql = "SELECT * FROM `reserva_tempo` WHERE data_servico='$data' ORDER BY horario asc ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function horariosPorData($data)
    {
        $sql = "SELECT horario FROM `reserva_tempo` WHERE data_servico='$data' ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function horariosPorHora($tempo, $data)
    {
        $sql = "SELECT nome, atendimento, servico, horario, observacao FROM `reserva_tempo` WHERE horario='$tempo' AND data_servico='$data' ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }


    public function atendimento($data)
    {
        $sql = "SELECT * FROM `reserva_tempo` WHERE data_servico='$data' AND status!='Finalizado' AND status!='Perdeu a vez' ORDER BY horario asc";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
    

    public function lista($id)
    {
        $sql = "SELECT * FROM `reserva` WHERE id=$id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function horarioocupado($horario, $data)
    {
        $sql = "SELECT nome, horario, servico, observacao, data_servico, atendimento FROM `reserva_tempo` WHERE horario='$horario' && data_servico='$data'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function verificarhorario($horario, $data)
    {
        $sql = "SELECT id FROM `reserva_tempo` WHERE horario='$horario' AND data_servico='$data'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function atualizarStatus($id, $status, $inicioAtendimento )
    {
        $sql = "update reserva_tempo set status = '".$status."', inicio_atendimento = '".$inicioAtendimento."' where id = ".$id;
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    
    }

    public function atualizar($id, $nome, $servico, $observacao, $atendente )
    {
        $sql = "update reserva set nome = '".$nome."', servico = '".$servico."',observacao = '".$observacao."',atendente = '".$atendente."' where id = ".$id;
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    
    }


    public function inserir($nome,$servico,$observacao)
    {
        $sql = "INSERT INTO reserva (nome, servico, observacao, atendente) VALUES (:nome, :servico,  :observacao, :atendente)";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome' => $nome, ':servico' => $servico, ':observacao' => $observacao, 'atendente' => $atendente);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function inserirCliente($nome,$servico,$observacao, $atendente,$horario,$data )
    {
        $sql = "INSERT INTO reserva_tempo (nome, servico, observacao, horario, atendimento, data_servico) VALUES (:nome, :servico,  :observacao, :horario, :atendimento, :data_servico)";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome' => $nome, ':servico' => $servico, ':observacao' => $observacao,':horario' => $horario, 'atendimento' => $atendente,':data_servico' => $data);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function deletar($id)
    {
        $sql = "DELETE FROM reserva WHERE id = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

}
