<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Enviado para análise. Muito Obrigado</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Erro ao enviar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title">Depoimento</h4>
          <span class="ajuda" data-tip="texto." tabindex="1"><i class="mdi mdi-help "></i></span>
          <?php if(!empty($depoimento)){?>
          <form id="formularios">
            <div class="row">
              <div class="col-md-6 ">
                <div class="form-group">
                  <label>Profissão</label>
                  <input type="text" name="profissao" class="form-control" value="<?php echo $depoimento[0]->profissao;?>">
                </div>
                <div class="form-group">
                  <label>Depoimento</label>
                  <textarea name="depoimento" class="contador-caracteres form-control" rows="4" maxlength="70"><?php echo $depoimento[0]->depoimento;?></textarea>
                  <span class="caracter"><span class="contadorCaracter">0</span> / 70</span> 
                </div>
                <div class="form-group">
                  <label>Avaliação</label>
                  <div class="stars">
                    <input type="radio" name="avaliacao" value="1" class="star-1" id="star-1" <?php if($depoimento[0]->avaliacao == 1){echo 'checked';};?>>
                    <label class="star-1" for="star-1">1</label>
                    <input type="radio" name="avaliacao" value="2" class="star-2" id="star-2" <?php if($depoimento[0]->avaliacao == 2){echo 'checked';};?>>
                    <label class="star-2" for="star-2">2</label>
                    <input type="radio" name="avaliacao" value="3" class="star-3" id="star-3" <?php if($depoimento[0]->avaliacao == 3){echo 'checked';};?>>
                    <label class="star-3" for="star-3">3</label>
                    <input type="radio" name="avaliacao" value="4" class="star-4" id="star-4" <?php if($depoimento[0]->avaliacao == 4){echo 'checked';};?>>
                    <label class="star-4" for="star-4">4</label>
                    <input type="radio" name="avaliacao" value="5" class="star-5" id="star-5" <?php if($depoimento[0]->avaliacao == 5){echo 'checked';};?>>
                    <label class="star-5" for="star-5">5</label>
                    <span></span>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <?php } else {?>
          <form id="formularios">
            <div class="row">
              <div class="col-md-6 ">
                <div class="form-group">
                  <label>Profissão</label>
                  <input type="text" name="profissao" class="form-control">
                </div>
                <div class="form-group">
                  <label>Depoimento</label>
                  <textarea name="depoimento" class="contador-caracteres form-control" rows="4" maxlength="70"></textarea>
                  <span class="caracter"><span class="contadorCaracter">0</span> / 70</span> 
                </div>
                <div class="form-group">
                  <label>Avaliação</label>
                  <div class="stars">
                    <input type="radio" name="avaliacao" value="1" class="star-1" id="star-1">
                    <label class="star-1" for="star-1">1</label>
                    <input type="radio" name="avaliacao" value="2" class="star-2" id="star-2">
                    <label class="star-2" for="star-2">2</label>
                    <input type="radio" name="avaliacao" value="3" class="star-3" id="star-3">
                    <label class="star-3" for="star-3">3</label>
                    <input type="radio" name="avaliacao" value="4" class="star-4" id="star-4">
                    <label class="star-4" for="star-4">4</label>
                    <input type="radio" name="avaliacao" value="5" class="star-5" id="star-5" checked>
                    <label class="star-5" for="star-5">5</label>
                    <span></span>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <?php };?>
        </div>
      </div>
    </div>
    <div class="col-md-3 grid-margin">
      <div class="card menu-atualizar">
        <div class="card-body">
          <h4 class="card-title">Menu depoimento</h4>
          <div class="media">
            <i class="mdi mdi-backup-restore icon-md d-flex align-self-start mr-3"></i>
            <div class="media-body d-flex align-self-center">
              <p class="card-text">Atualizado </p>
            </div>
          </div>
          <div class="media">
            <span class="d-flex align-self-start mr-3">
              <img src="<?php echo URL;?>images/faces/face4.jpg" alt="profile"
                style="border-radius: 100%;border: 2px solid #ececec;width: 30px;">
            </span>
            <div class="media-body d-flex align-self-center">
              <p class="card-text">Editado por </p>
            </div>
          </div>
          <?php if(!empty($depoimento)){?>
          <button destino="painelcliente/atualizarDepoimento" class="novo-ajax btn btn-inverse-primary btn-rounded btn-fw">
            Atualizar
          </button>
          <?php } else {?>
          <button destino="painelcliente/inserirDepoimento" class="novo-ajax btn btn-inverse-success btn-rounded btn-fw">
            Salvar
          </button>
          <?php }?>
        </div>
      </div>
    </div>
  </div>
</div>

