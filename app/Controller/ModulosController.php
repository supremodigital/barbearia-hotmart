<?php

namespace App\Controller;

use App\Model\Modulo;

class ModulosController
{
    public function index()
    {
        $modulo = new Modulo();
        $listaModulo = $modulo->listaTodos();

        $tagTitle = "Lista de Modulo";
		$tagDescricao = "";

        require APP . 'view/templates/header.php';
        require APP . 'view/modulo/index.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function novo()
    {
        $tagTitle = "Novo Modulo";
        $tagDescricao = "";

        require APP . 'view/templates/header.php';
        require APP . 'view/modulo/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function inserir()
    {
        $imagem = $_FILES['imagem'];

        $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/modulos');    
        $handle = new \Verot\Upload\Upload($imagem);

        //$ext = pathinfo($imagem, PATHINFO_EXTENSION);
        //$nome = $imagem['name'];

        if ($handle->uploaded)
        {
            $handle->image_resize         = true;
            $handle->image_x              = 100;
            $handle->image_ratio_y        = true;
            $handle->file_safe_name = false;
            $handle->file_name_body_add = '_mini';
            $handle->process($diretorio_destino);

        }

        if ($handle->uploaded)
        {
            $handle->image_resize         = true;
            $handle->image_x              = 300;
            $handle->image_ratio_y        = true;
            $handle->process($diretorio_destino);

        }

        $img = $handle->file_src_name;

        if (empty($img)) {
            $img = 'usuario.png';
        } else {
            $img = $handle->file_src_name;
        }

        $modulo = new Modulo();
        $msgModal = $modulo->inserir(
            $_POST["nome"],
            $_POST["descricao"],
            $_POST["link"],
            $img);
 
        echo json_encode($msgModal);
    }
    
    public function atualizar($id)
    {
        $imagem = $_FILES['imagem'];

        $modulo = new Modulo();
        $modulo = $modulo->lista($id);

        $imgBanco = $modulo[0]->img;
        $handle = new \Verot\Upload\Upload($imagem);
        $imgInput = $handle->file_src_name;
    
        if (!empty($imgInput)) { //Se tiver imagem input
            
            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/modulos');    
            $handle = new \Verot\Upload\Upload($imagem);
    
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 100;
                $handle->image_ratio_y        = true;
                $handle->file_safe_name = false;
                $handle->file_name_body_add = '_mini';
                $handle->process($diretorio_destino);
        
            }
        
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 680;
                $handle->image_ratio_y        = true;
                $handle->process($diretorio_destino);
            }

            $img = $handle->file_src_name;
            
        } else {
            $img = $imgBanco;
        } 
        
        $modulo = new Modulo();
        $msgModal = $modulo->atualizar(
            $id,
            $_POST["nome"],
            $_POST["descricao"],
            $_POST["link"],
            $img);

        echo json_encode($msgModal);    

    }

    public function editar($id)
    {
        $modulo = new Modulo();
        $moduloLista = $modulo->lista($id);

        $tagTitle = "Editar Modulo";
		$tagDescricao = "";

        require APP . 'view/templates/header.php';
        require APP . 'view/modulo/editar.php';
        require APP . 'view/templates/footer.php';
    }

    public function deletar($id)
    {
        $cliente = new Cliente();
        $cliente = $cliente->deletar($id);
        echo json_decode($cliente);

    }
}
