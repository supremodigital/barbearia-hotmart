<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Atualizado com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title">Novo Módulo</h4>
          <span class="ajuda" data-tip="texto." tabindex="1"><i class="mdi mdi-help "></i></span>
          <form id="formularios">
            <div class="row">
              <div class="col-md-6 ">
                <div class="form-group">
                  <label>Imagem</label>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="profile"
                      style="background-image: url('<?php echo URL; ?>images/img-upload.png');">
                        <label class="edit">
                          <span><i class="mdi mdi-upload"></i></span>
                          <input type="file" size="32" name="imagem" id="inputImagem">
                        </label>
                      </div>
                      <button type="button" id="editarImagem" class="btn btn-outline-primary btn-sm">Escolher</button>
                      <button type="button" id="removerImagem" class="btn btn-outline-danger btn-sm">Remover</button>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Nome</label>
                  <input type="text" name="moduloNome" class="form-control" value="">
                </div>
                <div class="form-group">
                  <label>Descrição</label>
                  <input type="text" name="moduloDescricao" class="form-control" value="">
                </div>
                <div class="form-group">
                  <label>Valor</label>
                  <input type="text" name="moduloValor" class="form-control" value="">
                </div>
                <div class="form-group">
                  <label>Valor promocional</label>
                  <input type="text" name="moduloPromocional" class="form-control" value="">
                </div>
                <div class="form-group">
                  <label>Link hotmart</label>
                  <input type="text" name="moduloLink" class="form-control" value="">
                </div>
              </div>
              <div class="col-md-6 ">
                        
              </div>
              <div class="col-md-6 ">
                <div class="form-group">
                  <button destino="modulos/inserir" class="novo-ajax btn btn-inverse-success btn-rounded btn-fw">
                    Salvar
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>