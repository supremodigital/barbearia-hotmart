$( "#ProfissionalEscolha" ).change(function() {
  var evento = this;
  var img = $('#ProfissionalEscolha option:selected').attr('imagem');
  $('#profissionalFoto').attr('src', img);
});

// PRELOAD
$( window ).on("load", function() {
  $('.preload').fadeOut( "slow", function() {
    $('.preload').hide();
 });
});

// BOTAO TOGGLE
$('.btn-toggle').on('click', function () {
  var vddFalse = $(this).attr('aria-pressed');
  if (vddFalse == 'false') {
    $(this).children('input').attr('checked', true);
  } else {
    $(this).children('input').attr('checked', false);
  }
});

// Modal
$(document).ready(function () {
  $('.restaurar-modal').on('click', function () {
    var idobjeto = $(this).attr('idobjeto');
    var data = $(this).attr('data');
    $("#data").text(data);
    $('#linkobjeto').attr('href', idobjeto);
  });

  $('.deletar-modal').on('click', function () {
    var idobjeto = $(this).attr('idobjeto');
    var msg = $(this).attr('msg');
    var destino = $(this).attr('destino');
    $("#descricaodeletar").text(msg);
    $('#linkdeletar').attr('idobjeto', idobjeto);
    $('#linkdeletar').attr('destino', destino);
  });

  $('.alerta-modal').on('click', function () {
    var idobjeto = $(this).attr('idobjeto');
    var msg = $(this).attr('msg');
    var destino = $(this).attr('destino');
    $("#descricao-alerta").text(msg);
    $('#link-alerta').attr('idobjeto', idobjeto);
    $('#link-alerta').attr('destino', destino);
  });
  $(document).on("click",".alerta-modal-carregado", function(){
    var idobjeto = $(this).attr('idobjeto');
    var msg = $(this).attr('msg');
    var destino = $(this).attr('destino');
    $("#descricao-alerta").text(msg);
    $('#link-alerta').attr('idobjeto', idobjeto);
    $('#link-alerta').attr('detino', destino);

  });
  $(document).on("click",".deletar-modal-carregado", function(){
    var idobjeto = $(this).attr('idobjeto');
    var msg = $(this).attr('msg');
    var destino = $(this).attr('destino');
    $("#descricaodeletar").text(msg);
    $('#linkdeletar').attr('idobjeto', idobjeto);
    $('#linkdeletar').attr('destino', destino);

  });

});

// CALENDARIO


$(function () {
  $(".calendar").datepicker({
    dateFormat: 'dd/mm/yy',
    firstDay: 1,

    closeText: 'Fechar',
      prevText: '&#x3c;Anterior',
      nextText: 'Pr&oacute;ximo&#x3e;',
      currentText: 'Hoje',
      monthNames: ['Janeiro', 'Fevereiro', 'Mar&ccedil;o', 'Abril', 'Maio', 'Junho',
        'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
        'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
      dayNames: ['Domingo', 'Segunda-feira', 'Ter&ccedil;a-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sabado'],
      dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
      dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
      weekHeader: 'Sm',
      dateFormat: 'dd/mm/yy',
      firstDay: 0,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ''
  },
  $.datepicker.regional['pt-BR']
  );

  //var date = new Date('2020/04/09','2020/04/15'); // Dashes won't work
//$('.calendar').datepicker('setDate', queryDate);
  //$('.calendar').datepicker("addDates", new Date(2020,9,03) );

  $('.ui-datepicker-next.ui-corner-all[data-handler="next"]').click(function (e) { 
    e.preventDefault();
dia();
    
  });
function dia() {
  var datas = document.querySelectorAll('td[data-handler="selectDay"]');

  //Dias do mês vindo do PHP
  var dias = [15, 22, 28];

  //Total de dias
  datas.length;
  //dia
  datas[0].innerText;

  //mes
  datas[0].dataset.month

  //ano
  datas[0].dataset.year

  // efeito marcar

  for (i = 0; i < dias.length; i++) {
    var dia = dias[i]-1;
    datas[dia].style.background = "#f3f3f3";
  }
}
  


  $(".date-picker .input").click(function (e) { 
    var $me = $(this),
      $parent = $me.parents('.date-picker');
    $parent.toggleClass('open');
    
  });

  $(".calendar").on("change", function () {
    var $me = $(this),
      $selected = $me.val(),
      $parent = $me.parents('.date-picker');
    $parent.find('.result').children('span').html($selected);
    
  });
});

// MOSTRAR SENHA
$(document).ready(function () {

  var ativo = true;
  $(".olhar-senha").click(function (e) {
    e.preventDefault();
    if (ativo) {
      var i = $('.olhar-senha i');
      i.removeClass('mdi-eye-off');
      i.addClass('mdi-eye text-primary');
      $('.input-senha').attr('type', 'text');
      ativo = !ativo;
    }
    else {
      var i = $('.olhar-senha i');
      i.removeClass('mdi-eye text-primary');
      i.addClass('mdi-eye-off');
      $('.input-senha').attr('type', 'password');
      ativo = !ativo;
    }

  });

});

// CHECKED SERVIÇOS
(function ($) {
  var checkboxes = $('.checkboxes input[type="checkbox"]').find(''),
    actions = $('.checkboxes-options > a');

  actions.on('click', function (e) {
    e.preventDefault();
    var $this = $(this);

    $this.parent().siblings().find('input[type="checkbox"]');

    if ($this.hasClass('check-all')) {
      //checkboxes.prop('checked', true)
      $this.parent().siblings().find('input[type="checkbox"]').prop('checked', true);
    }

    if ($this.hasClass('check-none')) {
      $this.parent().siblings().find('input[type="checkbox"]').prop('checked', false);
    }

    if ($this.hasClass('check-toggle')) {
      $this.parent().siblings().find('input[type="checkbox"]').prop('checked', $this.hasClass('checked'));
      $this.toggleClass('checked');
    }

  });
})(jQuery);

// Contador de caracter Meta TG
$(document).ready(function(){
    $('.contador-caracteres').siblings('.caracter').children().text($('.contador-caracteres').val().length);
});
$('.contador-caracteres').on('keyup keypress', function () {
  var teste = $(this).attr('maxlength');
  if (this.value.length > 70) {
    this.value = this.value.substring(0, limite);
  } else {
    $(this).siblings('.caracter').children().text(this.value.length);
  };

});

var valorMetaTitulo = $('#input-metatag-titulo').val();
if (typeof valorMetaTitulo !== 'undefined') {

  $('#input-metatag-titulo').siblings('.caracter').children().text(valorMetaTitulo.length);
  $('#titulo-meta-tag').text(valorMetaTitulo);

  $('#input-metatag-titulo').on('keyup keypress', function () {

    if (this.value.length > 70) {
      this.value = this.value.substring(0, limite);
    } else {
      $(this).siblings('.caracter').children().text(this.value.length);
      $('#titulo-meta-tag').text(this.value);
    };

  });

  var valorMetaDescri = $('#textarea-metatag-descricao').val();
  $('#textarea-metatag-descricao').siblings('.caracter').children().text(valorMetaDescri.length);
  $('#descricao-meta-tag').val(valorMetaDescri);

  $('#textarea-metatag-descricao').on('keyup keypress', function () {

    if (this.value.length > 156) {
      this.value = this.value.substring(0, limite);
    } else {
      $(this).siblings('.caracter').children().text(this.value.length);
      $('#descricao-meta-tag').val(this.value);
    };
  });
};

// upload imagem

var input = document.querySelector('#inputImagem');
if (typeof input !== 'undefined') {

  var imagem = document.querySelector('.profile');

  input.addEventListener('change', function (event) {
    var reader = new FileReader();
    reader.onload = function (e) {
      setImageUrl(e.target.result);
    };
    reader.readAsDataURL(event.target.files[0]);
  });

  function setImageUrl(url) {
    imagem.style.backgroundImage = 'url(' + url + ')';
  }
  $('#editarImagem').click(function () {
    $('#inputImagem').click();
  });

  $('#removerImagem').click(function () {
    setImageUrl('../images/img-upload.png');
  });
};

