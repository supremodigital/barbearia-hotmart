<?php

namespace App\Controller;

use App\Controller\NivelController;
use App\Model\Usuario;
use App\Model\Equipe;
use App\Model\NiveisUsuario;
use App\Controller\EmailController;
use Verot\Upload;

class UsuarioController
{

    public function __construct()
    {
        /*(new LoginController)->usuarioLongado();
                        
        $nivelAcesso = new NivelController();
        $nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);*/
    }

    public function index()
    {
        $usuarioLista = new Usuario();
        $usuarioLista = $usuarioLista->listaTodos();

        $tagTitle = "Usuário";
		$tagDescricao = "";

        require APP . 'view/templates/header.php';
        require APP . 'view/usuario/index.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function novo()
    {
        $nivelListaa = new NiveisUsuario();
        $nivelListaa = $nivelListaa->lista();

        require APP . 'view/usuario/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/usuario/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function editar($id)
    {
        $usuarioLista = new Usuario();
        $usuarioLista = $usuarioLista->lista($id);

        $nivelLista = new NiveisUsuario();
        $nivelLista = $nivelLista->lista();
                
        $tagTitle = "Editar usuário";
		$tagDescricao = "";

        require APP . 'view/templates/header.php';
        require APP . 'view/usuario/editar-usuario.php';
        require APP . 'view/templates/footer.php';
    }

    public function atualizarUsuario($id)
    {
        $usuario = new Usuario();
        $usuarios = $usuario->lista($id);

        $imagem = $_FILES['imagem'];

            $imgBanco = $usuarios[0]->img;
            $handle = new \Verot\Upload\Upload($imagem);
            $imgInput = $handle->file_src_name;
        
            if (!empty($imgInput)) { //Se tiver imagem input
                
                $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/usuario');    
                $handle = new \Verot\Upload\Upload($imagem);
        
                if ($handle->uploaded)
                {
                    $handle->image_resize         = true;
                    $handle->image_x              = 100;
                    $handle->image_ratio_y        = true;
                    $handle->file_safe_name = false;
                    $handle->file_name_body_add = '_mini';
                    $handle->process($diretorio_destino);
            
                }

                if ($handle->uploaded)
                {
                    $handle->image_resize         = true;
                    $handle->image_x              = 300;
                    $handle->image_ratio_y        = true;
                    $handle->process($diretorio_destino);
                }

                $img = $handle->file_src_name;
                
            } else {
                $img = $imgBanco;
            } 

            $usuario = new Usuario();
            $usuario = $usuario->atualizarUsuario($id, $_POST['nome'], $_POST['email'],$_POST['celular'],$_POST['telefone'],$_POST['aniversario'], $usuarios[0]->id_nivel, $img);

            echo json_decode($usuario);

    }

    public function atualizar($id)
    {
        $usuario = new Usuario();
        $usuario = $usuario->lista($id);

        var_dump($usuario);

        $senhadHash = $usuario[0]->senha;
        $senhaAntiga = $_POST['senha_antiga'];

        if (!empty($senhaAntiga)) {
            if (password_verify($senhaAntiga, $senhadHash)) {

                $senha = $_POST['nova_senha'];
                $senhadHash = password_hash($senha, PASSWORD_DEFAULT);
    
            } else {
                echo json_decode(0);
            }
        }

        $imagem = $_FILES['imagem'];

            $imgBanco = $usuario[0]->img;
            $handle = new \Verot\Upload\Upload($imagem);
            $imgInput = $handle->file_src_name;
        
            if (!empty($imgInput)) { //Se tiver imagem input
                
                $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/usuario');    
                $handle = new \Verot\Upload\Upload($imagem);
        
                if ($handle->uploaded)
                {
                    $handle->image_resize         = true;
                    $handle->image_x              = 100;
                    $handle->image_ratio_y        = true;
                    $handle->file_safe_name = false;
                    $handle->file_name_body_add = '_mini';
                    $handle->process($diretorio_destino);
            
                }
            
                if ($handle->uploaded)
                {
                    $handle->image_resize         = true;
                    $handle->image_x              = 300;
                    $handle->image_ratio_y        = true;
                    $handle->process($diretorio_destino);
            
                }

                $img = $handle->file_src_name;
                
            } else {
                $img = $imgBanco;
            } 

            $usuario = new Usuario();
            $usuario = $usuario->atualizar($id, $_POST['nome'], $_POST['email'],$_POST['celular'],$_POST['telefone'],$_POST['aniversario'], $usuario[0]->id_nivel, $senhadHash, $img);

            echo json_decode($usuario);

    }


    public function usuario($nomeEmail)
    { 
        $usuario = new Usuario();
        $usuario = $usuario->usuario($nomeEmail);
        
    }

    public function perfil()
    {

        $usuarioLista = new Usuario(__FUNCTION__);
        $usuarioLista = $usuarioLista->lista($_SESSION['idUsuario']);
        
        require APP . 'view/usuario/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/usuario/editar.php';
        require APP . 'view/templates/footer.php';
    }

    public function criarUsuario()
    {
        $nome = $_POST['nome'];
        $nivel = $_POST['nivel'];
        $email = $_POST['email'];

        //$usuario = new Usuario();
        //$usuario = $usuario->usuario($_SESSION['idUsuario']);

        $gerarSenha =rand();
        $senhadHash = password_hash($gerarSenha, PASSWORD_DEFAULT);

        $usuario = new Usuario();
        $msgModal = $cadastroUsuario = $usuario->criarUsuario($nome, $email, $senhadHash, $nivel, "usuario.jpg");

        $equipe = new Equipe();
        $cadastroEquipe = $equipe->inserirNovo($cadastroUsuario['id']);

        if ($msgModal) {
            $emailClass = new EmailController($_POST['nome'],$gerarSenha,"Novo Usuário",$_POST['email'],URL.'login','');
            $email = $emailClass->usuarioCadastrado();

            echo json_encode($email);
        } else {
            echo json_encode(0);
        }

    }

}

