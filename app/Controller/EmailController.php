<?php

namespace App\Controller;

use PHPMailer;

class EmailController
{
    private $template;
    private $email;
    private $assunto;
    private $nome;
    private $senha;
    private $destino;

    public function __construct($nome,$senha,$assunto,$email,$destino,$template)
    {
        $this->nome=$nome;
        $this->senha=$senha;
        $this->assunto=$assunto;
        $this->email=$email;
        $this->destino=$destino;
        $this->template=$template;
    }

    public function recuperarSenha()
    {
        $phpmail = new PHPMailer();

        $phpmail->isSMTP(); // envia por SMTP

        $phpmail->SMTPDebug = 0;
        $phpmail->Debugoutput = 'html';

        $phpmail->Host = "smtp.gmail.com"; // SMTP servers         
        $phpmail->Port = 587; // Porta SMTP do GMAIL

        $phpmail->SMTPSecure = 'tls'; // Autenticação SMTP
        $phpmail->SMTPAuth = true; // Caso o servidor SMTP precise de autenticação   

        $phpmail->Username = "goldtimegestao@gmail.com"; // SMTP username         
        $phpmail->Password = "Sup*Dig*84"; // SMTP password

        $nomeRemetente = '=?UTF-8?B?'.base64_encode('GoldTime Gestão').'?=';

        $phpmail->IsHTML(true);
       
        $phpmail->setFrom($this->email, $nomeRemetente); // E-mail do remetende enviado pelo method post  

        $phpmail->addAddress($this->email, $this->nome); // E-mail do destinatario/*  

        $phpmail->Subject = $this->assunto; // Assunto do remetende enviado pelo method post

        $body = str_replace(
            array(
              '[codigo]',
              '[nome]'
            ),
            array(
                $this->destino,
               $this->nome
            ),
            file_get_contents(APP . 'view/email/recuperar-senha.php')
        );
        //$body = file_get_contents(APP . 'view/email/recuperar-senha.php');

        $phpmail->msgHTML($body);

        if ($phpmail->send()) {
            return true;//echo json_encode(1);
        } else {
            return false;//echo json_encode(0);
        }
    }

    public function novoClienteCadastrado()
    {
        $phpmail = new PHPMailer();

        $phpmail->isSMTP(); // envia por SMTP

        $phpmail->SMTPDebug = 0;
        $phpmail->Debugoutput = 'html';

        $phpmail->Host = "smtp.gmail.com"; // SMTP servers         
        $phpmail->Port = 587; // Porta SMTP do GMAIL

        $phpmail->SMTPSecure = 'tls'; // Autenticação SMTP
        $phpmail->SMTPAuth = true; // Caso o servidor SMTP precise de autenticação   

        $phpmail->Username = "goldtimegestao@gmail.com"; // SMTP username         
        $phpmail->Password = "Sup*Dig*84"; // SMTP password

        $nomeRemetente = '=?UTF-8?B?'.base64_encode('GoldTime Gestão').'?=';

        $phpmail->IsHTML(true);
       
        $phpmail->setFrom($this->email, $nomeRemetente); // E-mail do remetende enviado pelo method post  

        $phpmail->addAddress($this->email, $this->nome); // E-mail do destinatario/*  

        $phpmail->Subject = $this->assunto; // Assunto do remetende enviado pelo method post

        $body = str_replace(
            array(
              '[codigo]',
              '[nome]'
            ),
            array(
                $this->destino,
               $this->nome
            ),
            file_get_contents(APP . 'view/email/novo-cliente.php')
        );
        //$body = file_get_contents(APP . 'view/email/recuperar-senha.php');

        $phpmail->msgHTML($body);

        if ($phpmail->send()) {
            return true;//echo json_encode(1);
        } else {
            return false;//echo json_encode(0);
        }
    }

    public function usuarioCadastrado()
    {
        $phpmail = new PHPMailer();

        $phpmail->isSMTP(); // envia por SMTP

        $phpmail->SMTPDebug = 0;
        $phpmail->Debugoutput = 'html';

        $phpmail->Host = "smtp.gmail.com"; // SMTP servers         
        $phpmail->Port = 587; // Porta SMTP do GMAIL

        $phpmail->SMTPSecure = 'tls'; // Autenticação SMTP
        $phpmail->SMTPAuth = true; // Caso o servidor SMTP precise de autenticação   

        $phpmail->Username = "goldtimegestao@gmail.com"; // SMTP username         
        $phpmail->Password = "Sup*Dig*84"; // SMTP password

        $nomeRemetente = '=?UTF-8?B?'.base64_encode('GoldTime Gestão').'?=';

        $phpmail->IsHTML(true);
       
        $phpmail->setFrom($this->email, $nomeRemetente); // E-mail do remetende enviado pelo method post  

        $phpmail->addAddress($this->email, $this->nome); // E-mail do destinatario/*  

        $phpmail->Subject = $this->assunto; // Assunto do remetende enviado pelo method post

        $body = str_replace(
            array(
              '[link]',
              '[senha]',
              '[nome]'
            ),
            array(
                $this->destino,
                $this->senha,
               $this->nome
            ),
            file_get_contents(APP . 'view/email/novo-usuario.php')
        );
        //$body = file_get_contents(APP . 'view/email/recuperar-senha.php');

        $phpmail->msgHTML($body);

        if ($phpmail->send()) {
            return true;//echo json_encode(1);
        } else {
            return false;//echo json_encode(0);
        }
    }

}
