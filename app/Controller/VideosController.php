<?php

namespace App\Controller;

use App\Model\Videos;
use App\Model\Modulo;

class VideosController
{
    public function index()
    {
        $video = new Videos();
        $videoLista = $video->listaTodoss();

        $tagTitle = "Vídeos";
		$tagDescricao = "";

        require APP . 'view/templates/header.php';
        require APP . 'view/videos/index.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function novo()
    {
        $modulosLista = new Modulo();
        $modulosLista = $modulosLista->listaTodos();

        require APP . 'view/videos/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/videos/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function inserir()
    {
        $videosLista = new Videos();
        $videosLista = $videosLista->inserir(
            $_POST['nome'],
            $_POST['descricao'],
            $_POST['link'],
            $_POST['modulo']
        );
 
        echo json_encode($videosLista);
    }

    public function editar($id)
    {
        $modulo = new Modulo();
        $moduloLista = $modulo->listaTodos();

        $video = new videos();
        $videoLista = $video->lista($id);

        $tagTitle = "Editar vídeo";
		$tagDescricao = "";

        require APP . 'view/templates/header.php';
        require APP . 'view/videos/editar.php';
        require APP . 'view/templates/footer.php';
    }

    public function atualizar($id)
    {
        $videosLista = new videos();
        $msgModal = $videosLista->atualizar(
            $id,
            $_POST["nome"],
            $_POST["descricao"],
            $_POST["link"],
            $_POST["modulo"]
        );

        echo json_encode($msgModal);    
    }

    public function deletar($id)
    {
        $cliente = new Cliente();
        $cliente = $cliente->deletar($id);
        echo json_decode($cliente);

    }
}
