<?php

namespace App\Model;

use App\Core\Model;

class Videos extends Model
{

    public function listaTodos()
    {
        $sql = "SELECT videos.*, modulo.moduloNome FROM `videos` inner join modulo in videos.idModulo == modulo.idModulo  WHERE 1 ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaTodoss()
    {
        $sql = "SELECT videos.idVideo, videos.videoNome, videos.videoDescricao, modulo.moduloNome FROM `videos` inner join modulo on videos.idModulo = modulo.idModulo";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function inserir($nome, $descricao, $link, $modulo)
    {
        $sql = "INSERT INTO videos (nomeVideo, descriVideo, linkVideo, idModulo) VALUES (:nomeVideo, :descriVideo, :linkVideo, :idModulo)";
        $query = $this->db->prepare($sql);
        $parameters = array(':nomeVideo' => $nome, ':descriVideo' => $descricao, ':linkVideo' =>$link, ':idModulo' => $modulo);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function lista($id)
    {
        $sql = "SELECT * FROM `videos` WHERE idVideo=$id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
    
    public function atualizar($id, $nome, $descricao, $link, $modulo)
    {
        $sql = "update videos set nomeVideo = '".$nome."', descriVideo = '".$descricao."',linkVideo = '".$link."',idModulo = '".$modulo."' where idVideo = ".$id;
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        } 
    }

    public function deletar($id)
    {
        $sql = "DELETE FROM cliente WHERE id = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

}
