<?php

namespace App\Model;

use App\Core\Model;

class ServicoMulher extends Model
{

    public function listaTodos()
    {
        $sql = "SELECT * FROM `servico_mulher` WHERE 1 ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function lista($id)
    {
        $sql = "SELECT * FROM `servico_mulher` WHERE id=$id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function atualizar($id, $nomeServico, $descricao, $categoria, $valor, $tempo, $img, $alt )
    {
        $sql = "update servico_mulher set nome_servico = '".$nomeServico."', descricao = '".$descricao."',categoria = '".$categoria."',valor = '".$valor."',tempo = '".$tempo."',img = '".$img."',alt = '".$alt."' where id = ".$id;
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    
    }

    public function inserir($nomeServico, $descricao, $categoria, $valor, $tempo, $img, $alt )
    {
        $sql = "INSERT INTO servico_mulher (nome_servico, descricao, categoria, valor, tempo, img, alt) VALUES (:nome_servico, :descricao, :categoria, :valor, :tempo, :img, :alt)";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome_servico' => $nomeServico, ':descricao' => $descricao, ':categoria' => $categoria, ':valor' => $valor, ':tempo' => $tempo, ':img' => $img, 'alt' => $alt);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function deletar($id)
    {
        $sql = "DELETE FROM servico_mulher WHERE id = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

}
