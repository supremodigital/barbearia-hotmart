<div class="content-wrapper container-sem-menu">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal modal-sem-menu d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Reserva cancelada.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal modal-sem-menu d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Erro ao reservar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title">Status de reserva</h4>
          <p class="card-description">
            Informação de sua reserva.
          </p>
          <div class="row">
            <div class="col-md-8 ">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Cabeleireiro</th>
                      <th>Posição fila</th>
                      <th>Data</th>
                      <th>Status</th>
                      <th>Tempo estimado</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><?php echo $existeReserva[0]->nome; ?></td>
                      <td><?php echo $posicaoFila; ?></td>
                      <td><?php echo $DataAtual; ?></td>
                      <td><label class="badge badge-warning"><?php echo $existeReserva[0]->estatus; ?></label></td>
                      <td>2:00</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <button type="button" class="deletar-modal btn btn-inverse-danger btn-fw" idobjeto="<?php echo $_SESSION['idCliente']; ?>" msg="reserva" destino="painelcliente/cancelarReserva/" data-toggle="modal" data-target="#deletarModal">Cancelar reserva</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Apagar modal -->
  <div class="modal fade" id="deletarModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-estilo">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Cancelamento da reserva</h4>
                <button type="button" class="btn btn-inverse-danger btn-icon" data-dismiss="modal" aria-label="Close">
                    <i class="mdi mdi-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <h5>Deseja realmente cancelar a <span id="descricaodeletar"></span>?</h5>
            </div>
            <div class="modal-footer">
                <button id="linkdeletar" destino="" idobjeto="" type="button" class="link-ajaxxx btn btn-outline-primary" data-dismiss="modal">Confirmar</button>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
  </div>
</div>