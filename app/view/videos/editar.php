<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Atualizado com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title">Novo Vídeo</h4>
          <span class="ajuda" data-tip="texto." tabindex="1"><i class="mdi mdi-help "></i></span>
          <?php foreach ($videoLista as $linha) { ?>
          <form id="formularios">
            <div class="row">
              <div class="col-md-6 ">
                <div class="form-group">
                  <label>Nome</label>
                  <input type="text" name="nome" class="form-control" placeholder="Nome" value="<?php echo $linha->videoNome;?>">
                </div>
                <div class="form-group">
                  <label>Descrição</label>
                  <textarea name="descricao" class="form-control" rows="4"><?php echo $linha->videoDescricao;?></textarea>
                </div>
              </div>
              <div class="col-md-6 ">
                <div class="form-group">
                  <label>Link Youtube</label>
                  <div class="input-group">
                    <div class="input-group-append">
                      <button class="btn btn-sm btn-youtube" type="button">
                        <i class="mdi mdi-youtube"></i>
                      </button>
                    </div>
                    <input type="text" name="link" class="form-control" value="<?php echo $linha->videoLink;?>">
                  </div>
                </div>
                <div class="form-group">
                  <label>Módulo</label>
                  <select class="form-control form-control-sm" name="modulo">
                  <?php foreach ($moduloLista as $linhas) { ?>
                      <option value="<?php echo $linhas->idModulo;?>"<?php if($videoLista[0]->idModulo == $linhas->idModulo){echo 'selected';};?>><?php echo $linhas->moduloNome;?></option>
                  <?php } ?>
                  </select>
                </div>        
              </div>
              <div class="col-md-6 ">
                <div class="form-group">
                  <button destino="videos/atualizar/<?php echo $linha->idVideo;?>" class="link-ajax-atualizar btn btn-inverse-primary btn-rounded btn-fw">
                    Atualizar
                  </button>
                </div>
              </div>
            </div>
          </form>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>