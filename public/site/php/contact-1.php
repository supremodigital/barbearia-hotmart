<?php
/*
THIS FILE USES PHPMAILER INSTEAD OF THE PHP MAIL() FUNCTION
*/

require 'PHPMailer-master/PHPMailerAutoload.php';

/*
*  CONFIGURE EVERYTHING HERE
*/

$Cliente = $_POST['nome'];
$ClienteEmail = $_POST['email'];

// an email address that will be in the From field of the email.
$fromEmail = $ClienteEmail;
$fromName = $Cliente;

// an email address that will receive the email with the output of the form
$sendToEmail = 'contato@grupo-morbeck.com' ;
$sendToName = 'Grupo Morbeck';

// subject of the email
$subject = 'Contato Site Grupo Morbeck';

// form field names and their translations.
// array variable name => Text to appear in the email
$fields = array('nome' => 'Nome','sobrenome' => 'Sobrenome', 'celular' => 'Celular', 'email' => 'Email');

// message that will be displayed when everything is OK :)
$okMessage = 'Inscrição realizada com sucesso, em breve nossa equipe entrará em contato!';

// If something goes wrong, we will display this message.
$errorMessage = 'Ocorreu um erro ao enviar o formulário. Por favor, tente novamente mais tarde';

/*
*  LET'S DO THE SENDING
*/

// if you are not debugging and don't need error reporting, turn this off by error_reporting(0);
error_reporting(E_ALL & ~E_NOTICE);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    function post_captcha($user_response) {
        $fields_string = '';
        $fields = array(
            'secret' => '6Le-dAAVAAAAAKJVBE5H5bQkHAoKoIxW_z8zRrN7',
            'response' => $user_response
        );
        foreach($fields as $key=>$value)
        $fields_string .= $key . '=' . $value . '&';
        $fields_string = rtrim($fields_string, '&');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);

        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result, true);
    }

    // Call the function post_captcha
    $res = post_captcha($_POST['g-recaptcha-response']);
    }

try
{

    if(count($_POST) == 0) throw new \Exception('Form is empty');

    $emailTextHtml = "<h1>Mensagem de pré inscrições de Cliente</h1><hr>";
    $emailTextHtml .= "<table>";

    foreach ($_POST as $key => $value) {
        // If the field exists in the $fields array, include it in the email
        if (isset($fields[$key])) {
            $emailTextHtml .= "<tr><th>$fields[$key]</th><td>$value</td></tr>";
        }
    }
    $emailTextHtml .= "</table><hr>";
    $emailTextHtml .= "<p></p>";

    $mail = new PHPMailer;

    $mail->setFrom($fromEmail, $fromName);
    $mail->addAddress($sendToEmail, $sendToName); // you can add more addresses by simply adding another line with $mail->addAddress();
    $mail->addReplyTo($from);

    $mail->isHTML(true);

    $mail->Subject = $subject;
    $mail->msgHTML($emailTextHtml); // this will also create a plain-text version of the HTML email, very handy

    if ($res['success']) {

        if(!$mail->send()) {
            throw new \Exception('I could not send the email.' . $mail->ErrorInfo);
        }

        $responseArray = array('type' => 'success', 'message' => $okMessage);
    }
    
}
catch (\Exception $e)
{
    // $responseArray = array('type' => 'danger', 'message' => $errorMessage);
    $responseArray = array('type' => 'danger', 'message' => $e->getMessage());
}


// if requested by AJAX request return JSON response
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $encoded = json_encode($responseArray);

    header('Content-Type: application/json');

    echo $encoded;
}
// else just display the message
else {
    echo $responseArray['message'];
}
