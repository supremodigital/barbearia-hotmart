<?php

namespace App\Model;

use App\Core\Model;

class Notificacao extends Model
{

    public function listaTodos()
    {
        $sql = "SELECT * FROM `notificacao` WHERE 1 ORDER by id DESC";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
    

    public function lista($id)
    {
        $sql = "SELECT * FROM `notificacao` WHERE id=$id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function quantidade()
    {
        $sql = "SELECT COUNT(*) AS notificacao FROM notificacao";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function inserir($categoria, $descricao)
    {
        $sql = "INSERT INTO notificacao (categoria, descricao) VALUES (:categoria, :descricao)";
        $query = $this->db->prepare($sql);
        $parameters = array(':categoria' => $categoria, ':descricao' => $descricao);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function deletar($id)
    {
        $sql = "DELETE FROM notificacao WHERE id = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

}
