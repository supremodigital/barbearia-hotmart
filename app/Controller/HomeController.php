<?php

namespace App\Controller;

use App\Model\Paginas;
use App\Model\Slider;
use App\Model\ServicoHomem;
use App\Model\CategoriaServico;
use App\Model\Equipe;
use App\Model\Depoimento;
use App\Model\Reserva;

class HomeController
{
    public function index()
    {
        require APP . 'view/site/index.php';
    }
}
