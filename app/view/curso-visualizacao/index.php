<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-atualizar">Atualizado com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-erro">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div id="corpo-card" class="card-body">

          <style>
            .material-icons {
              font-family: Material Icons;
              font-weight: 400;
              font-style: normal;
              font-size: 24px;
              display: inline-block;
              line-height: 1;
              text-transform: none;
              letter-spacing: normal;
              word-wrap: normal;
              white-space: nowrap;
              direction: ltr;
              -webkit-font-smoothing: antialiased;
              text-rendering: optimizeLegibility;
              -moz-osx-font-smoothing: grayscale;
              font-feature-settings: "liga";
              font-display: block;
            }

            *,
            :after,
            :before {
              box-sizing: border-box;
            }

            nav {
              display: block;
            }

            h1,
            p {
              margin-top: 0;
              margin-bottom: 1rem;
            }

            ul {
              margin-bottom: 1rem;
            }

            ul {
              margin-top: 0;
            }

            small {
              font-size: 80%;
            }

            a {
              color: rgba(39, 44, 51, .7);
              text-decoration: none;
              background-color: transparent;
            }

            a:hover {
              color: rgba(6, 7, 8, .7);
              text-decoration: underline;
            }

            img {
              border-style: none;
            }

            img {
              vertical-align: middle;
            }

            .h1,
            h1 {
              margin-bottom: 1rem;
              font-family: Exo\ 2, Helvetica Neue, Arial, sans-serif;
              font-weight: 600;
              line-height: 1.5;
              color: #303956;
            }
            .small,
            small {
              font-size: .8125rem;
              font-weight: 400;
            }

            .container {
              width: 100%;
              padding-right: 0px!important;
              padding-left: 0px!important;
              margin-right: auto;
              margin-left: auto;
            }


            .btn {
              display: inline-block;
              font-family: Exo\ 2, Helvetica Neue, Arial, sans-serif;
              font-weight: 600;
              color: #272c33;
              text-align: center;
              vertical-align: middle;
              -webkit-user-select: none;
              -moz-user-select: none;
              -ms-user-select: none;
              user-select: none;
              background-color: transparent;
              border: 1px solid transparent;
              padding: .5rem 1rem;
              font-size: .8125rem;
              line-height: 1.5;
              border-radius: .25rem;
              transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
            }

            @media (prefers-reduced-motion:reduce) {
              .btn {
                transition: none;
              }
            }

            .btn:hover {
              color: #272c33;
              text-decoration: none;
            }

            .btn:focus {
              outline: 0;
              box-shadow: 0 0 0 1px #5567ff;
            }

            .btn:disabled {
              opacity: .65;
              box-shadow: none;
            }

            .nav {
              display: flex;
              flex-wrap: wrap;
              padding-left: 0;
              margin-bottom: 0;
              list-style: none;
            }

            .navbar {
              position: relative;
              padding: .5rem 1rem;
            }

            .navbar,
            .navbar .container {
              display: flex;
              flex-wrap: wrap;
              align-items: center;
              justify-content: space-between;
            }

            .navbar-nav {
              display: flex;
              flex-direction: column;
              padding-left: 0;
              margin-bottom: 0;
              list-style: none;
            }

            @media (max-width:575.98px) {
              .navbar-expand-sm>.container {
                padding-right: 0;
                padding-left: 0;
              }
            }

            @media (min-width:576px) {
              .navbar-expand-sm {
                flex-flow: row nowrap;
                justify-content: flex-start;
              }

              .navbar-expand-sm .navbar-nav {
                flex-direction: row;
              }

              .navbar-expand-sm>.container {
                flex-wrap: nowrap;
              }
            }

            .navbar-expand {
              flex-flow: row nowrap;
              justify-content: flex-start;
            }

            .navbar-expand>.container {
              padding-right: 0;
              padding-left: 0;
            }

            .navbar-expand>.container {
              flex-wrap: nowrap;
            }

            .card-title {
              margin-bottom: 1rem;
            }

            .media {
              align-items: flex-start;
            }

            .media-body {
              flex: 1;
            }

            .rounded {
              border-radius: .25rem !important;
            }

            .rounded-circle {
              border-radius: 50% !important;
            }

            .d-flex {
              display: flex !important;
            }

            .embed-responsive {
              position: relative;
              display: block;
              width: 100%;
              padding: 0;
              overflow: hidden;
            }

            .embed-responsive:before {
              display: block;
              content: "";
            }

            .embed-responsive iframe {
              position: absolute;
              top: 0;
              bottom: 0;
              left: 0;
              width: 100%;
              height: 100%;
              border: 0;
            }

            .embed-responsive-16by9:before {
              padding-top: 56.25%;
            }

            .flex-column {
              flex-direction: column !important;
            }

            .flex-wrap {
              flex-wrap: wrap !important;
            }

            .flex-nowrap {
              flex-wrap: nowrap !important;
            }

            .justify-content-start {
              justify-content: flex-start !important;
            }

            .align-items-end {
              align-items: flex-end !important;
            }

            .align-items-center {
              align-items: center !important;
            }

            @media (min-width:576px) {
              .flex-sm-row {
                flex-direction: row !important;
              }

              .align-items-sm-center {
                align-items: center !important;
              }
            }

            .m-0 {
              margin: 0 !important;
            }

            .mb-0 {
              margin-bottom: 0 !important;
            }

            .mr-8pt {
              margin-right: .5rem !important;
            }

            .mr-16pt {
              margin-right: 1rem !important;
            }

            .mb-16pt {
              margin-bottom: 1rem !important;
            }

            .mb-24pt {
              margin-bottom: 1.5rem !important;
            }

            .mb-32pt {
              margin-bottom: 2rem !important;
            }

            .p-0 {
              padding: 0 !important;
            }

            @media (min-width:576px) {
              .mb-sm-0 {
                margin-bottom: 0 !important;
              }

              .mr-sm-16pt {
                margin-right: 1rem !important;
              }

              .ml-sm-auto {
                margin-left: auto !important;
              }
            }

            @media (min-width:576px) {
              .text-sm-center {
                text-align: center !important;
              }
            }

            .font-weight-light {
              font-weight: 300 !important;
            }

            .font-weight-bold {
              font-weight: 500 !important;
            }

            .title-video  {
              color: #d7a428;
            }

            .text-body {
              color: #272c33 !important;
            }

            .text-muted {
              color: rgba(39, 44, 51, .5) !important;
            }

            @media print {

              *,
              :after,
              :before {
                text-shadow: none !important;
                box-shadow: none !important;
              }

              a:not(.btn) {
                text-decoration: underline;
              }

              img {
                page-break-inside: avoid;
              }

              p {
                orphans: 3;
                widows: 3;
              }

              .container {
                min-width: 992px !important;
              }

              .navbar {
                display: none;
              }
            }

            .mdk-header-layout__content {
              position: relative;
              z-index: 0;
            }

            .flex {
              flex: 1 1 0% !important;
            }

            .lh-1 {
              line-height: 1rem;
            }

            .text-50 {
              color: rgba(39, 44, 51, .5) !important;
            }

            .text-white-50 {
              color: #303956!important;
            }

            .icon--left {
              margin-right: .5rem;
            }

            .icon--right {
              margin-left: .5rem;
            }



            .bg-white {
              background-color: #fff !important;
            }

            .border-bottom-2 {
              border-bottom: 2px solid #e9edf2 !important;
            }

            .border-0 {
              border: 0 !important;
            }

            .material-icons {
              vertical-align: middle;
            }

            .h1,
            h1 {
              line-height: 1.5;
            }

            .small,
            small {
              line-height: 1.5;
            }

            a:hover {
              text-decoration: none;
            }

            .measure-hero-lead {
              max-width: 696px;
            }



            .card-title {
              font-size: 1rem;
              font-family: Exo\ 2, Helvetica Neue, Arial, sans-serif;
              font-weight: 600;
              color: #303956;
              line-height: 1.25;
              margin-bottom: 0;
            }

            .card-title {
              font-weight: 500;
            }

            .card-title[href] {
              color: inherit;
              display: flex;
              text-decoration: none;
            }

            .rating,
            .rating__item {
              display: flex;
              align-items: center;
            }

            .rating__item {
              color: #f9c32c;
            }

            .rating .material-icons {
              font-size: 1rem;
            }

            .rating-24 .material-icons {
              font-size: 1.5rem;
            }

            .media-left {
              display: flex;
              flex-direction: column;
            }

            .media-left {
              margin-right: .5rem;
            }

            .media-body {
              flex: 1 1 0%;
            }

            .media {
              display: flex;
              flex-flow: row wrap;
            }

            .layout-sticky-subnav .page__container {
              z-index: 0;
              position: relative;
            }

            @media (min-width:992px) {
              .hero__lead {
                font-size: 1.414rem;
                line-height: 1.414;
              }

              .hero__lead:not(:last-child) {
                margin-bottom: 2rem;
              }
            }

            .navbar {
              min-height: 64px;
              padding-top: 0;
              padding-bottom: 0;
            }

            @media (max-width:767.98px) {
              .navbar .container {
                max-width: none;
              }
            }

            @media (max-width:575.98px) {
              .navbar-expand-sm.navbar-list {
                height: auto;
              }

              .navbar-expand-sm.navbar-list .navbar-list__item {
                padding: 1rem;
                width: 100%;
              }

              .navbar-expand-sm.navbar-list.navbar-light .navbar-list__item:not(:last-child) {
                border-bottom: 1px solid #e9edf2;
              }
            }

            @media (min-width:576px) {
              .navbar-expand-sm .navbar-nav .nav-item {
                display: flex;
                align-items: center;
              }

              .navbar-expand-sm .navbar-nav .nav-item+.nav-item {
                margin-left: 1rem;
              }
            }

            .navbar-list {
              padding-left: 0;
              padding-right: 0;
            }

            .navbar-list__item {
              padding-top: .5rem;
              padding-bottom: .5rem;
            }

            .course-nav {
              display: flex;
              justify-content: space-between;
              width: 100%;
              margin-bottom: 2rem;
              position: relative;
              z-index: 0;
            }

            .course-nav:before {
              content: "";
              position: absolute;
              left: 0;
              right: 0;
              top: calc(50% - 1px);
              height: 2px;
              background-color: #fff;
              opacity: .2;
              z-index: -1;
            }

            .course-nav a {
              display: flex;
              align-items: center;
              justify-content: center;
              width: 2rem;
              height: 2rem;
              border-radius: 100px;
              background-color: #f5f7fa;
            }

            .course-nav a .material-icons {
              font-size: 1rem;
              color: rgba(39, 44, 51, .7);
            }

            /*! CSS Used fontfaces */
            @font-face {
              font-family: Material Icons;
              font-style: normal;
              font-weight: 400;
              src: url(http://localhost/weepy/luma.humatheme.com/assets/fonts/material-icons/MaterialIcons-Regular.eot);
              src: local("\263A"), url(http://localhost/weepy/luma.humatheme.com/assets/fonts/material-icons/MaterialIcons-Regular.woff2) format("woff2"), url(http://localhost/weepy/luma.humatheme.com/assets/fonts/material-icons/MaterialIcons-Regular.woff) format("woff"), url(http://localhost/weepy/luma.humatheme.com/assets/fonts/material-icons/MaterialIcons-Regular.ttf) format("truetype");
            }

            @font-face {
              font-family: 'Exo 2';
              font-style: normal;
              font-weight: 600;
              font-display: swap;
              src: url(https://fonts.gstatic.com/s/exo2/v9/7cH1v4okm5zmbvwkAx_sfcEuiD8jYPWsNNC_nps.woff2) format('woff2');
              unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
            }

            @font-face {
              font-family: 'Exo 2';
              font-style: normal;
              font-weight: 600;
              font-display: swap;
              src: url(https://fonts.gstatic.com/s/exo2/v9/7cH1v4okm5zmbvwkAx_sfcEuiD8jYPWsPdC_nps.woff2) format('woff2');
              unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
            }

            @font-face {
              font-family: 'Exo 2';
              font-style: normal;
              font-weight: 600;
              font-display: swap;
              src: url(https://fonts.gstatic.com/s/exo2/v9/7cH1v4okm5zmbvwkAx_sfcEuiD8jYPWsNtC_nps.woff2) format('woff2');
              unicode-range: U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;
            }

            @font-face {
              font-family: 'Exo 2';
              font-style: normal;
              font-weight: 600;
              font-display: swap;
              src: url(https://fonts.gstatic.com/s/exo2/v9/7cH1v4okm5zmbvwkAx_sfcEuiD8jYPWsN9C_nps.woff2) format('woff2');
              unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
            }

            @font-face {
              font-family: 'Exo 2';
              font-style: normal;
              font-weight: 600;
              font-display: swap;
              src: url(https://fonts.gstatic.com/s/exo2/v9/7cH1v4okm5zmbvwkAx_sfcEuiD8jYPWsOdC_.woff2) format('woff2');
              unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
            }

            .container.page__container {}
          </style>

            <div class="mdk-header-layout__content page-content " style="/* padding-top: 64px; */">
              <div class="pb-lg-64pt py-32pt">
                <div class="container page__container">

                  <div class="js-player embed-responsive embed-responsive-16by9 mb-32pt" data-domfactory-upgraded="player">
                  <iframe id="teste" width="560" height="315" src="https://www.youtube-nocookie.com/embed/1F0DVgk64uk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>

                  <div class="d-flex flex-wrap align-items-end mb-16pt">
                    <h1 class="title-video flex m-0">Introduction to TypeScript</h1>
                    <p class="h1 text-white-50 font-weight-light m-0">50:13</p>
                  </div>

                  <p class="hero__lead measure-hero-lead text-white-50 mb-24pt">JavaScript is now used to power backends, create hybrid mobile applications, architect cloud solutions, design neural networks and even control robots. Enter TypeScript: a superset of JavaScript for scalable, secure, performant and feature-rich applications.</p>

                  <div class="d-flex flex-column flex-sm-row align-items-center justify-content-start">


                  </div>
                </div>
              </div>
              <div class="navbar navbar-expand-sm navbar-light bg-white border-bottom-2 navbar-list p-0 m-0 align-items-center">
                <div class="container page__container">
                  <ul class="nav navbar-nav flex align-items-sm-center">
                    <li class="nav-item navbar-list__item">
                      <div class="media align-items-center">
                        <span class="media-left mr-16pt">
                          <img src="assets/images/people/50/guy-6.jpg" width="40" alt="avatar" class="rounded-circle">
                        </span>
                        <div class="media-body">
                          <a class="card-title m-0" href="fixed-teacher-profile.html">Eddie Bryan</a>
                          <p class="text-50 lh-1 mb-0">Instructor</p>
                        </div>
                      </div>
                    </li>
                    <li class="nav-item navbar-list__item">
                      <i class="material-icons text-muted icon--left">schedule</i>
                      2h 46m
                    </li>
                    <li class="nav-item navbar-list__item">
                      <i class="material-icons text-muted icon--left">assessment</i>
                      Beginner
                    </li>
                    <li class="nav-item ml-sm-auto text-sm-center flex-column navbar-list__item">
                      <div class="rating rating-24">
                        <div class="rating__item"><i class="material-icons">star</i></div>
                        <div class="rating__item"><i class="material-icons">star</i></div>
                        <div class="rating__item"><i class="material-icons">star</i></div>
                        <div class="rating__item"><i class="material-icons">star</i></div>
                        <div class="rating__item"><i class="material-icons">star_border</i></div>
                      </div>
                      <p class="lh-1 mb-0"><small class="text-muted">20 ratings</small></p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>




            <div id="accordion">
              <div class="card">
                <div class="card-header" id="headingOne">
                  <h5 class="mb-0">
                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      Collapsible Group Item #1
                    </button>
                  </h5>
                </div>

                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                  </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="headingTwo">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      Collapsible Group Item #2
                    </button>
                  </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                  <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                  </div>
                </div>
              </div>
              <div class="card">
                <div class="card-header" id="headingThree">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      Collapsible Group Item #3
                    </button>
                  </h5>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                  <div class="card-body">
                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                  </div>
                </div>
              </div>
            </div>


        </div>
      </div>
    </div>
  </div>
</div>
