<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-atualizar">Atualizado com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-erro">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div id="corpo-card" class="card-body">
          <h4 class="card-title">Cursos</h4>
          <span class="ajuda" data-tip="texto." tabindex="1"><i class="mdi mdi-help "></i></span>
          <a href="cliente/novo" class="btn btn-inverse-secondary btn-rounded btn-fw" style="float:right">
            Novo
          </a>
          <p class="card-description">
            Lista de seus cursos comprados.
          </p>
          <style>
            /*! CSS Used from: http://localhost/weepy/luma.humatheme.com/assets/css/material-icons.css */
.material-icons{font-family:Material Icons;font-weight:400;font-style:normal;font-size:24px;display:inline-block;line-height:1;text-transform:none;letter-spacing:normal;word-wrap:normal;white-space:nowrap;direction:ltr;-webkit-font-smoothing:antialiased;text-rendering:optimizeLegibility;-moz-osx-font-smoothing:grayscale;font-feature-settings:"liga";font-display:block;}
/*! CSS Used from: http://localhost/weepy/luma.humatheme.com/assets/css/app.css */
*,:after,:before{box-sizing:border-box;}
p{margin-top:0;margin-bottom:1rem;}
small{font-size:80%;}
a{color:rgba(39,44,51,.7);text-decoration:none;background-color:transparent;}
a:hover{color:rgba(6,7,8,.7);text-decoration:underline;}
img{border-style:none;}
img{vertical-align:middle;}
.small,small{font-size:.8125rem;font-weight:400;}
.row{display:flex;flex-wrap:wrap;margin-right:-12px;margin-left:-12px;}
.col-auto,.col-lg-3{position:relative;width:100%;padding-right:12px;padding-left:12px;}
.col-auto{flex:0 0 auto;width:auto;max-width:100%;}
@media (min-width:992px){
.col-lg-3{flex:0 0 25%;max-width:25%;}
}
.btn{/* display:inline-block; */font-family:Exo\ 2,Helvetica Neue,Arial,sans-serif;/* font-weight:600; *//* color:#272c33; *//* text-align:center; *//* vertical-align:middle; *//* -webkit-user-select:none; */-moz-user-select:none;-ms-user-select:none;/* user-select:none; *//* background-color:transparent; *//* border:1px solid transparent; *//* padding:.5rem 1rem; *//* font-size:.8125rem; *//* line-height:1.5; *//* border-radius:.25rem; *//* transition:color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out; */}
@media (prefers-reduced-motion:reduce){
.btn{transition:none;}
}
.btn:hover{color:#272c33;text-decoration:none;}
.btn:focus{outline:0;box-shadow:0 0 0 1px #5567ff;}
.btn:disabled{opacity:.65;box-shadow:none;}
.btn-primary{color:#fff;background-color:#5567ff;border-color:#5567ff;box-shadow:inset 0 1px 0 hsla(0,0%,100%,.15),0 1px 1px rgba(39,44,51,.075);}
.btn-primary:focus,.btn-primary:hover{color:#fff;background-color:#2f45ff;border-color:#2239ff;}
.btn-primary:focus{box-shadow:inset 0 1px 0 hsla(0,0%,100%,.15),0 1px 1px rgba(39,44,51,.075),0 0 0 1px rgba(111,126,255,.5);}
.btn-primary:disabled{color:#fff;background-color:#5567ff;border-color:#5567ff;}
.btn-outline-secondary{color:#868e96;border-color:#868e96;}
.btn-outline-secondary:hover{color:#fff;background-color:#868e96;border-color:#868e96;}
.btn-outline-secondary:focus{box-shadow:0 0 0 1px rgba(134,142,150,.5);}
.btn-outline-secondary:disabled{color:#868e96;background-color:transparent;}
.card{position:relative;display:flex;flex-direction:column;min-width:0;word-wrap:break-word;background-color:#fff;background-clip:border-box;border:1px solid #dfe2e6;border-radius:.5rem;}
.card-body{/* flex:1 1 auto; *//* min-height:1px; *//* padding:1rem; */}
.card-title{margin-bottom:1rem;}
.card-footer{padding:1rem;background-color:#fff;}
.card-footer:last-child{border-radius:0 0 .5rem .5rem;}
.card-img-top{flex-shrink:0;width:100%;}
.card-img-top{border-top-left-radius:.5rem;border-top-right-radius:.5rem;}
.media{align-items:flex-start;}
.media-body{flex:1;}
.rounded{border-radius:.25rem!important;}
.d-none{display:none!important;}
.d-flex{display:flex!important;}
.flex-column{flex-direction:column!important;}
.justify-content-center{justify-content:center!important;}
.justify-content-between{justify-content:space-between!important;}
.align-items-center{align-items:center!important;}
.mb-0{margin-bottom:0!important;}
.ml-0{margin-left:0!important;}
.mr-4pt{margin-right:.25rem!important;}
.mb-4pt{margin-bottom:.25rem!important;}
.ml-4pt{margin-left:.25rem!important;}
.mr-8pt{margin-right:.5rem!important;}
.mb-8pt{margin-bottom:.5rem!important;}
.mr-12pt{margin-right:.75rem!important;}
.my-16pt{margin-top:1rem!important;}
.mb-16pt,.my-16pt{margin-bottom:1rem!important;}
.my-32pt{margin-top:2rem!important;}
.my-32pt{margin-bottom:2rem!important;}
.text-center{text-align:center!important;}
.font-weight-bold{font-weight:500!important;}
.text-white{color:#fff!important;}
.text-primary{color:#5567ff!important;}
.text-black-50{color:rgba(39,44,51,.5)!important;}
@media print{
*,:after,:before{text-shadow:none!important;box-shadow:none!important;}
a:not(.btn){text-decoration:underline;}
img{page-break-inside:avoid;}
p{orphans:3;widows:3;}
}
.corner-ribbon{margin:0;text-align:center;white-space:nowrap;position:absolute;overflow:hidden;font-weight:500;box-sizing:border-box;}
.corner-ribbon--default-right-top{height:30px;width:100px;padding-right:31px;padding-left: 21px;line-height:30px;font-size: 14px;}
.corner-ribbon--default-right-top{transform-origin:0 100%;transform:rotate(45deg);right:-30px;top:-30px;}
.corner-ribbon--shadow{box-shadow:0 0 3px rgba(0,0,0,.3);}
.flex{flex:1 1 0%!important;}
.lh-1{line-height:1rem;}
.text-20{color:rgba(39,44,51,.2)!important;}
.text-50,.text-black-50{color:rgba(39,44,51,.5)!important;}
.text-black-70{color:rgba(39,44,51,.7)!important;}
.o-hidden{overflow:hidden!important;}
.bg-accent{background-color:#ed0b4c!important;}
.material-icons{vertical-align:middle;}
.icon-16pt{font-size:1rem!important;}
.icon-32pt{font-size:2rem!important;}
.small,small{line-height:1.5;}
a:hover{text-decoration:none;}
.btn{/* display:inline-flex; *//* align-items:center; *//* justify-content:center; *//* text-transform:uppercase; */}
.card{margin-bottom:1.5rem;}
.card{box-shadow:0 3px 3px -2px rgba(39,44,51,.1),0 3px 4px 0 rgba(39,44,51,.04),0 1px 8px 0 rgba(39,44,51,.02);transition:box-shadow .28s cubic-bezier(.4,0,.2,1);will-change:box-shadow;}
.card--elevated:hover{box-shadow:0 3px 5px -1px rgba(39,44,51,.1),0 5px 8px 0 rgba(39,44,51,.04),0 1px 14px 0 rgba(39,44,51,.02);}
.card-sm .card-body,.card-sm .card-footer{padding:.625rem .75rem;}
.card-title{font-size:1rem;font-family:Exo\ 2,Helvetica Neue,Arial,sans-serif;font-weight:600;color:#303956;line-height:1.25;margin-bottom:0;}
.card-group-row__col{display:flex;}
.card-group-row__card{flex:1 0 0;}
.card-title{font-weight:500;}
.card-title[href]{color:inherit;display:flex;text-decoration:none;}
.card-title:last-child{margin-bottom:0;}
.card-footer{border-top:1px solid #dfe2e6;padding:.75rem;}
.card-footer:last-child{border-bottom-right-radius:.5rem;border-bottom-left-radius:.5rem;}
.rating,.rating__item{display:flex;align-items:center;}
.rating__item{color:#f9c32c;}
.rating .material-icons{font-size:1rem;}
.media-left{display:flex;flex-direction:column;}
.media-left{margin-right:.5rem;}
.media-body{flex:1 1 0%;}
.media{display:flex;flex-flow:row wrap;}
.overlay{position:relative;}
.overlay__content{position:absolute;left:0;top:0;bottom:0;right:0;transition:opacity .4s,background-color .4s;display:flex;align-items:center;justify-content:center;color:#fff;pointer-events:none;}
.overlay__action{transition:opacity .4s,transform .4s;opacity:0;transform:translate3d(0,10px,0);}
.overlay--show .overlay__content{opacity:1;}
.overlay--show .overlay__action{opacity:1;transform:translateZ(0);}
.overlay--primary-dodger-blue .overlay__content{background-color:rgba(85,103,255,.35);}
.overlay--primary-dodger-blue.overlay--show .overlay__content{background-color:rgba(85,103,255,.95);}
/*! CSS Used fontfaces */
@font-face{font-family:Material Icons;font-style:normal;font-weight:400;src:url(http://localhost/weepy/luma.humatheme.com/assets/fonts/material-icons/MaterialIcons-Regular.eot);src:local("\263A"),url(http://localhost/weepy/luma.humatheme.com/assets/fonts/material-icons/MaterialIcons-Regular.woff2) format("woff2"),url(http://localhost/weepy/luma.humatheme.com/assets/fonts/material-icons/MaterialIcons-Regular.woff) format("woff"),url(http://localhost/weepy/luma.humatheme.com/assets/fonts/material-icons/MaterialIcons-Regular.ttf) format("truetype");}
@font-face{font-family:'Exo 2';font-style:normal;font-weight:600;font-display:swap;src:url(https://fonts.gstatic.com/s/exo2/v9/7cH1v4okm5zmbvwkAx_sfcEuiD8jYPWsNNC_nps.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
@font-face{font-family:'Exo 2';font-style:normal;font-weight:600;font-display:swap;src:url(https://fonts.gstatic.com/s/exo2/v9/7cH1v4okm5zmbvwkAx_sfcEuiD8jYPWsPdC_nps.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
@font-face{font-family:'Exo 2';font-style:normal;font-weight:600;font-display:swap;src:url(https://fonts.gstatic.com/s/exo2/v9/7cH1v4okm5zmbvwkAx_sfcEuiD8jYPWsNtC_nps.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;}
@font-face{font-family:'Exo 2';font-style:normal;font-weight:600;font-display:swap;src:url(https://fonts.gstatic.com/s/exo2/v9/7cH1v4okm5zmbvwkAx_sfcEuiD8jYPWsN9C_nps.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
@font-face{font-family:'Exo 2';font-style:normal;font-weight:600;font-display:swap;src:url(https://fonts.gstatic.com/s/exo2/v9/7cH1v4okm5zmbvwkAx_sfcEuiD8jYPWsOdC_.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
          </style>
          <div class="row card-group-row">
            <div class="col-lg-3 card-group-row__col">
              <div class="card card-sm card--elevated p-relative o-hidden overlay overlay--primary-dodger-blue js-overlay card-group-row__card" data-toggle="popover" data-trigger="click" data-original-title="" title="" data-domfactory-upgraded="overlay">
                <a href="student-take-course.html" class="card-img-top js-image" data-position="center" data-height="140" data-domfactory-upgraded="image" style="display: block; position: relative; overflow: hidden; background-image: url('site/images/galley-images/gallery-img1-big.jpg'); background-size: cover; background-position: center center; height: 140px;">
                  <span class="overlay__content">
                    <span class="overlay__action d-flex flex-column text-center">
                      <i class="material-icons icon-32pt">play_circle_outline</i>
                      <span class="card-title text-white">Resume</span>
                    </span>
                  </span>
                </a>
                <span class="corner-ribbon corner-ribbon--default-right-top corner-ribbon--shadow bg-accent text-white">Em Breve</span>
                <div class="card-body flex">
                  <div class="d-flex">
                    <div class="flex">
                      <a class="card-title" href="student-take-course.html">Tesoura de ouro</a>
                      <small class="text-50 font-weight-bold mb-4pt">Avaliação</small>
                    </div>
                    <a href="student-take-course.html" data-toggle="tooltip" data-title="Add Favorite" data-placement="top" data-boundary="window" class="ml-4pt material-icons text-20 card-course__icon-favorite" data-original-title="" title="">favorite_border</a>
                  </div>
                  <div class="d-flex">
                    <div class="rating flex">
                      <span class="rating__item"><span class="material-icons">star</span></span>
                      <span class="rating__item"><span class="material-icons">star</span></span>
                      <span class="rating__item"><span class="material-icons">star</span></span>
                      <span class="rating__item"><span class="material-icons">star</span></span>
                      <span class="rating__item"><span class="material-icons">star_border</span></span>
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="row justify-content-between">
                    <div class="col-auto d-flex align-items-center">
                      <span class="material-icons icon-16pt text-black-50 mr-4pt">access_time</span>
                      <p class="flex text-black-50 lh-1 mb-0"><small>6 horas</small></p>
                    </div>
                    <div class="col-auto d-flex align-items-center">
                      <span class="material-icons icon-16pt text-black-50 mr-4pt">play_circle_outline</span>
                      <p class="flex text-black-50 lh-1 mb-0"><small>6 vídeos</small></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 card-group-row__col">
              <div class="card card-sm card--elevated p-relative o-hidden overlay overlay--primary-dodger-blue js-overlay card-group-row__card" data-toggle="popover" data-trigger="click" data-original-title="" title="" data-domfactory-upgraded="overlay">
                <a href="student-take-course.html" class="card-img-top js-image" data-position="center" data-height="140" data-domfactory-upgraded="image" style="display: block; position: relative; overflow: hidden; background-image: url('site/images/galley-images/gallery-img1-big.jpg'); background-size: cover; background-position: center center; height: 140px;">
                  <span class="overlay__content">
                    <span class="overlay__action d-flex flex-column text-center">
                      <i class="material-icons icon-32pt">play_circle_outline</i>
                      <span class="card-title text-white">Resume</span>
                    </span>
                  </span>
                </a>
                <span class="corner-ribbon corner-ribbon--default-right-top corner-ribbon--shadow bg-accent text-white">Em Breve</span>
                <div class="card-body flex">
                  <div class="d-flex">
                    <div class="flex">
                      <a class="card-title" href="student-take-course.html">Tesoura de ouro</a>
                      <small class="text-50 font-weight-bold mb-4pt">Avaliação</small>
                    </div>
                    <a href="student-take-course.html" data-toggle="tooltip" data-title="Add Favorite" data-placement="top" data-boundary="window" class="ml-4pt material-icons text-20 card-course__icon-favorite" data-original-title="" title="">favorite_border</a>
                  </div>
                  <div class="d-flex">
                    <div class="rating flex">
                      <span class="rating__item"><span class="material-icons">star</span></span>
                      <span class="rating__item"><span class="material-icons">star</span></span>
                      <span class="rating__item"><span class="material-icons">star</span></span>
                      <span class="rating__item"><span class="material-icons">star</span></span>
                      <span class="rating__item"><span class="material-icons">star_border</span></span>
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="row justify-content-between">
                    <div class="col-auto d-flex align-items-center">
                      <span class="material-icons icon-16pt text-black-50 mr-4pt">access_time</span>
                      <p class="flex text-black-50 lh-1 mb-0"><small>6 horas</small></p>
                    </div>
                    <div class="col-auto d-flex align-items-center">
                      <span class="material-icons icon-16pt text-black-50 mr-4pt">play_circle_outline</span>
                      <p class="flex text-black-50 lh-1 mb-0"><small>6 vídeos</small></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>