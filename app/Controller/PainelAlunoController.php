<?php

namespace App\Controller;

use App\Model\Cliente;
use App\Model\Usuario;
use App\Model\Depoimento;
use App\Controller\LoginController;
use Verot\Upload;

class PainelalunoController
{
    private $nome;
    private $email;
    private $senha;
    private $IdCabeleireiro;
    private $servico;

    public function __construct()
    {
        //(new LoginController)->clienteLongado();

        //$nivelAcesso = new NivelController();
        //$nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
    }

    public function index()
    {
        require APP . 'view/cliente/head.php';
        require APP . 'view/templates/header-aluno.php';
        require APP . 'view/cursos-cliente/index.php';
        require APP . 'view/templates/footer.php';
    }

    public function modulo()
    {
        require APP . 'view/cliente/head.php';
        require APP . 'view/templates/header-aluno.php';
        require APP . 'view/cursos-cliente/index.php';
        require APP . 'view/templates/footer.php';
    }

    public function curso()
    {
        require APP . 'view/cliente/head.php';
        require APP . 'view/templates/header-aluno.php';
        require APP . 'view/curso-visualizacao/index.php';
        require APP . 'view/templates/footer.php';
    }

    
}
