<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Atualizado com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title">Novo Vídeo</h4>
          <span class="ajuda" data-tip="texto." tabindex="1"><i class="mdi mdi-help "></i></span>
          <form id="formularios">
            <div class="row">
              <div class="col-md-6 ">
                <div class="form-group">
                  <label>Nome</label>
                  <input type="text" name="nome" class="form-control" placeholder="Nome" value="">
                </div>
                <div class="form-group">
                  <label>Descrição</label>
                  <textarea name="descricao" class="form-control" rows="4"></textarea>
                </div>
              </div>
              <div class="col-md-6 ">
                <div class="form-group">
                  <label>Link Youtube</label>
                  <div class="input-group">
                    <div class="input-group-append">
                      <button class="btn btn-sm btn-youtube" type="button">
                        <i class="mdi mdi-youtube"></i>
                      </button>
                    </div>
                    <input type="text" name="link" class="form-control" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label>Módulo</label>
                  <select class="form-control form-control-sm" name="modulo">
                    <?php foreach ($modulosLista as $linha) { ?>
                    <option value="<?php echo $linha->idModulo;?>"><?php echo $linha->nomeModulo;?></option>
                    <?php } ?>
                  </select>
                </div>        
              </div>
              <div class="col-md-6 ">
                <div class="form-group">
                  <button destino="videos/inserir" class="novo-ajax btn btn-inverse-success btn-rounded btn-fw">
                    Salvar
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>