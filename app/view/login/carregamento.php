<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        HTML CSSResult * {
            box-sizing: border-box;
        }

        .carregamento {
            background: #ebebeb;
            overflow: hidden;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
        }

        .container {
            width: 500px;
            margin: 0 auto;
        }

        .pole {
            height: 335px;
            width: 130px;
            position: relative;
        }

        .head {
            position: absolute;
            width: 120px;
            height: 120px;
            left: 50%;
            margin-left: -60px;
            border: 7px solid #5C496D;
            border-radius: 50%;
            background: #DADCE2;
            overflow: hidden;
            padding-top: 6px;
            box-shadow: inset 0 -60px 0 -30px #9993A8;
        }

        .head:after {
            content: "";
            width: 70px;
            height: 50px;
            left: 50%;
            top: 5px;
            margin-left: -35px;
            background: #E7EBEB;
            border-radius: 50%;
            position: absolute;
        }

        .head-base {
            position: absolute;
            top: 90px;
            left: 50%;
            margin-left: -50px;
            height: 16px;
            border: 7px solid #5C496D;
            border-bottom: none;
            width: 100px;
            z-index: 2;
            background: #DADCE2;
            box-shadow: inset 0 -4px 0 0 #9993A8;
        }

        .loader-head {
            position: absolute;
            height: 38px;
            width: 100%;
            border: 7px solid #5C496D;
            top: 106px;
            z-index: 2;
            background: #9993A8;
            box-shadow: inset 0 6px 0 0 #DADCE2;
        }

        .loader {
            position: absolute;
            height: 177px;
            width: 102px;
            top: 124px;
            left: 50%;
            margin-left: -51px;
            border: 7px solid #5C496D;
            background: #E7EBEB;
            z-index: 3;
            overflow: hidden;
        }

        .loader:after {
            content: "";
            width: 50%;
            height: 177px;
            background: #83738F;
            position: absolute;
            top: 0;
            right: 0;
            opacity: 0.2;
        }

        .loader .inset {
            border-right: 6px solid #E7EBEB;
            border-left: 6px solid #E7EBEB;
            width: 100%;
            position: relative;
            overflow: hidden;
            animation: spin 1s infinite linear;
        }

        .loader .inset div {
            transform: rotate(-40deg);
        }

        .loader .red {
            height: 28px;
            width: 200%;
            margin-bottom: 42px;
            background: #E33035;
            position: relative;
            left: -50%;
        }

        .loader .blue {
            height: 28px;
            width: 200%;
            margin-bottom: 42px;
            background: #1f08ff;
            position: relative;
            left: -50%;
        }

        .loader-base {
            position: absolute;
            height: 38px;
            width: 100%;
            border: 7px solid #5C496D;
            bottom: 16px;
            z-index: 2;
            background: #DADCE2;
            box-shadow: inset 0 -6px 0 0 #9993A8;
        }

        .base {
            position: absolute;
            bottom: 0;
            left: 50%;
            margin-left: -50px;
            height: 16px;
            border: 7px solid #5C496D;
            border-top: none;
            width: 100px;
            z-index: 2;
            background: #DADCE2;
            box-shadow: inset 0 -4px 0 0 #9993A8;
        }

        @keyframes spin {
            from {
                transform: translateY(0);
            }

            to {
                transform: translateY(-140px);
            }
        }
    </style>
</head>

<body>
    <div class="carregamento">
    <div class='pole'>
        <div class='head'></div>
        <div class='head-base'></div>
        <div class='loader-head'></div>
        <div class='loader'>
            <div class='inset'>
                <div class='red'></div>
                <div class='blue'></div>
                <div class='red'></div>
                <div class='blue'></div>
                <div class='red'></div>
                <div class='blue'></div>
            </div>
        </div>
        <div class='loader-base'></div>
        <div class='base'></div>
    </div>
    </div>
    <script>setTimeout(function(){ $('.carregamento').fadeTo( "slow", 0 ); }, 2500);</script>
    <script src="../site3/js/jquery-1.12.4.min.js"></script>

</body>

</html>