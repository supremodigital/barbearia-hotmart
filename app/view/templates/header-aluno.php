<!DOCTYPE html>
<html lang="pt-br">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $tagTitle;?></title>
  <meta name="description" content="<?php echo $tagDescricao;?>">
	<!-- plugins:css -->
	<link rel="stylesheet" href="<?php echo URL; ?>vendors/mdi/css/materialdesignicons.min.css">
	<link rel="stylesheet" href="<?php echo URL; ?>vendors/base/vendor.bundle.base.css">
	<!-- endinject -->
	<!-- plugin css for this page -->
	<link rel="stylesheet" href="<?php echo URL; ?>vendors/datatables.net-bs4/dataTables.bootstrap4.css">
	<!-- End plugin css for this page -->
	<!-- inject:css -->
	<link rel="stylesheet" href="<?php echo URL; ?>css/style.css">
	<!-- endinject -->
	<link rel="shortcut icon" href="images/favicon.png" />
	<link rel="stylesheet" href="<?php echo URL; ?>css/goldtime.css">
	
</head>
	
<?php
  use App\Model\Nivel;
  use App\Model\Usuario;

  //$notificacaoLista = new NotificacaoController();
  //$notificacaoLista = $notificacaoLista->notificacao();

  //$nivel = strtolower($_SESSION['nivelUsuario']);
  //$nivelLista = new Nivel();
  //$nivelLista = $nivelLista->listaNivel("$nivel");

  //foreach ( $nivelLista as $linha) {
    //$controles[] = $linha->controler;
  //}

  $usuario = new Usuario();
  $usuario = $usuario->lista($_SESSION['idUsuario']);

?>

<body>
<div class="preload"></div>
  <div class="container-scroller">
    <!-- INICIO HEADER -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="navbar-brand-wrapper d-flex justify-content-center">
            <div class="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100">
            <a class="navbar-brand brand-logo" href="<?php echo URL; ?>"><img src="<?php echo URL; ?>images/logo.png" alt="logo" /></a>
            <a class="navbar-brand brand-logo-mini" href="<?php echo URL; ?>"><img src="<?php echo URL; ?>images/logo-mini.png" alt="logo" /></a>
            <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                <span class="mdi mdi-sort-variant"></span>
            </button>
            </div>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
            <!--<"ul class="navbar-nav mr-lg-4 w-100">
                <li class="nav-item nav-search d-none d-lg-block w-100">
                    <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="search">
                        <i class="mdi mdi-magnify"></i>
                        </span>
                    </div>
                    <input type="text" class="form-control" placeholder="Search now" aria-label="search" aria-describedby="search">
                    </div>
                </li>
                </ul>-->
            <ul class="navbar-nav navbar-nav-right">
            <!--<li class="nav-item dropdown mr-1">
                    <a class="nav-link count-indicator dropdown-toggle d-flex justify-content-center align-items-center" id="messageDropdown" href="#" data-toggle="dropdown">
                    <i class="mdi mdi-message-text mx-0"></i>
                    <span class="count"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="messageDropdown">
                    <p class="mb-0 font-weight-normal float-left dropdown-header">Messages</p>
                    <a class="dropdown-item">
                        <div class="item-thumbnail">
                            <img src="images/faces/face1.jpg" alt="image" class="profile-pic">
                        </div>
                        <div class="item-content flex-grow">
                        <h6 class="ellipsis font-weight-normal">David Grey
                        </h6>
                        <p class="font-weight-light small-text text-muted mb-0">
                            The meeting is cancelled
                        </p>
                        </div>
                    </a>
                    <a class="dropdown-item">
                        <div class="item-thumbnail">
                            <img src="images/faces/face1.jpg" alt="image" class="profile-pic">
                        </div>
                        <div class="item-content flex-grow">
                        <h6 class="ellipsis font-weight-normal">Tim Cook
                        </h6>
                        <p class="font-weight-light small-text text-muted mb-0">
                            New product launch
                        </p>
                        </div>
                    </a>
                    <a class="dropdown-item">
                        <div class="item-thumbnail">
                            <img src="images/faces/face3.jpg" alt="image" class="profile-pic">
                        </div>
                        <div class="item-content flex-grow">
                        <h6 class="ellipsis font-weight-normal"> Johnson
                        </h6>
                        <p class="font-weight-light small-text text-muted mb-0">
                            Upcoming board meeting
                        </p>
                        </div>
                    </a>
                    </div>
                </li>-->
            <!--<li class="nav-item dropdown mr-4">
                <a class="noti nav-link count-indicator dropdown-toggle d-flex align-items-center justify-content-center notification-dropdown" id="notificationDropdown" href="#" data-toggle="dropdown">
                <i class="mdi mdi-bell mx-0"></i>
                <span class="count" style="display:none"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="notificationDropdown">
                <p class="mb-0 font-weight-normal dropdown-header">Notificações</p>
                <div id="notificacao-ajax" qtd="<?php echo count($notificacaoLista); ?>">
                <?php foreach ($notificacaoLista as $linha) { ?>

                <a class="dropdown-item">
                  <div class="item-thumbnail">
                    <div class="item-icon bg-success">
                      <i class="mdi mdi-information mx-0"></i>
                    </div>
                  </div>
                  <div class="item-content">
                    <h6 class="font-weight-normal"><?php echo $linha->categoria; ?></h6>
                    <p class="font-weight-light small-text mb-0 text-muted">
                      <?php echo $linha->descricao; ?>
                    </p>
                  </div>
                </a>              
                  
                <?php } ?>
                
                </div>
                <audio id="audio" src="<?php echo URL; ?>som/notificacao.mp3"></audio>
                <a class="dropdown-item">
                    <div class="item-thumbnail">
                    <div class="item-icon bg-success">
                        <i class="mdi mdi-information mx-0"></i>
                    </div>
                    </div>
                    <div class="item-content">
                    <h6 class="font-weight-normal">Categoria</h6>
                    <p class="font-weight-light small-text mb-0 text-muted">
                        Nome
                    </p>
                    </div>
                </a>
                <a class="dropdown-item">
                    <div class="item-thumbnail">
                    <div class="item-icon bg-warning">
                        <i class="mdi mdi-settings mx-0"></i>
                    </div>
                    </div>
                    <div class="item-content">
                    <h6 class="font-weight-normal">Categoria</h6>
                    <p class="font-weight-light small-text mb-0 text-muted">
                        Nome
                    </p>
                    </div>
                </a>
                <a class="dropdown-item">
                    <div class="item-thumbnail">
                    <div class="item-icon bg-info">
                        <i class="mdi mdi-account-box mx-0"></i>
                    </div>
                    </div>
                    <div class="item-content">
                    <h6 class="font-weight-normal">Novo contato</h6>
                    <p class="font-weight-light small-text mb-0 text-muted">
                        nome
                    </p>
                    </div>
                </a>
                </div>
            </li>-->
            <li class="nav-item nav-profile dropdown">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                <img src="<?php echo URL; ?>images/usuario/<?php $img = explode('.',$usuario[0]->usuarioImg); echo $img[0] . "_mini." . $img[1]; ?>" alt="profile" /> 
                <span class="nav-profile-name"><?php echo $usuario[0]->usuarioNome?></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                <a href="<?php echo URL; ?>perfil" class="dropdown-item">
                    <i class="mdi mdi-account-outline text-primary"></i>
                    Perfil
                </a>
                <a class="dropdown-item" href="<?php echo URL; ?>login/sairUsuario">
                    <i class="mdi mdi-power text-primary"></i>
                    Sair
                </a>
                </div>
            </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
            data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
            </button>
        </div>
    </nav>
    <!-- FIM HEADER -->
    
    <!-- INICIO Container principal -->
    <div class="container-fluid page-body-wrapper">

      <!-- INICIO SIDEBAR -->
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>videos">     
              <i class="mdi mdi-message-video menu-icon"></i>
              <span class="menu-title">Vídeos</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>modulos">        
              <i class="mdi mdi-view-grid menu-icon"></i>
              <span class="menu-title">Modulos</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>depoimento">
              <i class="mdi mdi-comment-account-outline menu-icon"></i>
              <span class="menu-title">Depoimentos</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>cliente">
              <i class="mdi mdi-account-outline menu-icon"></i>
              <span class="menu-title">Clientes</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>usuario">
              <i class="mdi mdi-account-circle menu-icon"></i>
              <span class="menu-title">Usuários</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URL; ?>login/sairUsuario">
              <i class="mdi mdi-power menu-icon"></i>
              <span class="menu-title">Sair</span>
            </a>
          </li>
        </ul>
      </nav>
      <!-- FIM SIDEBAR -->
      <div id="main" class="main-panel">
        <div class="carregar-pagina">
          <svg class="spinner" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
          <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
          </svg>
        </div>
