<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-atualizar">Atualizado com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-erro">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
    <?php foreach ($informacaoLista as $linha) { ?>
      <form id="formularios">
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title">Informação de contato</h4>
          <span class="ajuda" data-tip="texto." tabindex="1"><i class="mdi mdi-help "></i></span>
          <p class="card-description">
            informações básicas de contato
          </p>
            <div class="row">
              <div class="col-md-6 ">
                <div class="row">
                  <div class="col-md-9">
                    <div class="form-group">
                      <label>Endereço</label>
                      <input type="text" name="endereco" class="form-control" value="<?php echo $linha->endereco;?>">
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label>Número</label>
                      <input type="text" name="numero" class="form-control" value="<?php echo $linha->numero;?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-8">
                    <div class="form-group">
                      <label>Bairro</label>
                      <input type="text" name="bairro" class="form-control" value="<?php echo $linha->bairro;?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>CEP</label>
                      <input type="text" name="cep" class="form-control" value="<?php echo $linha->cep;?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <label>Horário de atendimento</label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Abertura</label>
                      <input type="time" name="abertura" class="input-hora form-control" value="<?php echo $linha->abertura;?>">
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Fechamento</label>
                      <input type="time" name="fechamento" class="input-hora form-control" value="<?php echo $linha->fechamento;?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-8">
                    <label>E-mail</label>
                    <input type="text" name="email" class="form-control" value="<?php echo $linha->email;?>">
                  </div>
                </div>
              </div>
              <div class="col-md-6 ">
                <div class="row">
                  <div class="col-md-9">
                    <div class="form-group">
                      <label>Telefone</label>
                      <input type="text" name="telefone" class="form-control" placeholder="(DDD) 0000-0000" value="<?php echo $linha->telefone;?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-9">
                    <div class="form-group">
                      <label>Celular</label>
                      <input type="text" name="celular" class="form-control" placeholder="(DDD) 90000-0000" value="<?php echo $linha->celular;?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-9">
                    <div class="form-group">
                      <label>WhatsApp</label>
                      <input type="text" name="whatsapp" class="form-control" placeholder="(DDD) 90000-0000" value="<?php echo $linha->whatsapp;?>">
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title">Redes sociais</h4>
          <p class="card-description">
            informações básicas de contato
          </p>
          <div class="row">
            <div class="col-md-6 ">
              <div class="row">
                <div class="col-md-9">
                  <div class="form-group">
                    <label>Facebook</label>
                    <div class="input-group">
                      <div class="input-group-append">
                        <button class="btn btn-sm btn-facebook" type="button">
                          <i class="mdi mdi-facebook"></i>
                        </button>
                      </div>
                      <input type="text" name="facebook" class="form-control" placeholder="" value="<?php echo $linha->facebook;?>">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-9">
                  <div class="form-group">
                    <label>Instagram</label>
                    <div class="input-group">
                      <div class="input-group-append">
                        <button class="btn btn-sm btn-instagram" type="button">
                          <i class="mdi mdi-instagram"></i>
                        </button>
                      </div>
                      <input type="text" name="instagram" class="form-control" value="<?php echo $linha->instagram;?>">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-9">
                  <div class="form-group">
                    <label>Twitter</label>
                    <div class="input-group">
                      <div class="input-group-append">
                        <button class="btn btn-sm btn-twitter" type="button">
                          <i class="mdi mdi-twitter"></i>
                        </button>
                      </div>
                      <input type="text" name="twitter" class="form-control" value="<?php echo $linha->twitter;?>">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="row">
                <div class="col-md-9">
                  <div class="form-group">
                    <label>Youtube</label>
                    <div class="input-group">
                      <div class="input-group-append">
                        <button class="btn btn-sm btn-youtube" type="button">
                          <i class="mdi mdi-youtube"></i>
                        </button>
                      </div>
                      <input type="text" name="youtube" class="form-control" value="<?php echo $linha->youtube;?>">
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-9">
                  <div class="form-group">
                    <label>Linkedin</label>
                    <div class="input-group">
                      <div class="input-group-append">
                        <button class="btn btn-sm btn-linkedin" type="button">
                          <i class="mdi mdi-linkedin"></i>
                        </button>
                      </div>
                      <input type="text" name="linkedin" class="form-control" value="<?php echo $linha->linkedin;?>">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </form>
    <?php } ?>
    </div>
    <div class="col-md-3 grid-margin">
      <div class="card menu-atualizar">
        <div class="card-body">
          <h4 class="card-title">Menu informações</h4>
          <div class="media">
            <i class="mdi mdi-backup-restore icon-md d-flex align-self-start mr-3"></i>
            <div class="media-body d-flex align-self-center">
              <p class="card-text">Atualizado dia 04/11/2019</p>
            </div>
          </div>
          <div class="media">
            <span class="d-flex align-self-start mr-3">
              <img src="images/faces/face4.jpg" alt="profile"
                style="border-radius: 100%;border: 2px solid #ececec;width: 30px;">
            </span>
            <div class="media-body d-flex align-self-center">
              <p class="card-text">Editado por Nome</p>
            </div>
          </div>
          <button destino="informacao/atualizar/1" class="link-ajax-atualizar btn btn-inverse-primary  btn-rounded btn-fw">
            Atualizar
          </button>
        </div>
      </div>
    </div>
  </div>
</div>