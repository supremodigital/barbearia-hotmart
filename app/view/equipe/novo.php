<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Atualizado com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM -->
  <div class="row">
  
    <div class="col-md-9 grid-margin">
    <form id="formularios">
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title">Adicionar um novo membro</h4>
          <span class="ajuda" data-tip="texto." tabindex="1"><i class="mdi mdi-help "></i></span>
          <p class="card-description">
          </p>
            <div class="row">
              <div class="col-md-6 ">
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <div class="profile" style="background-image: url('<?php echo URL;?>images/usuario.png')">
                          <label class="edit">
                            <span><i class="mdi mdi-upload"></i></span>
                            <input type="file" size="32" name="imagem" id="inputImagem">
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <button type="button" id="editarImagem" class="btn btn-outline-primary btn-sm">Adicionar</button>
                        <button type="button" id="removerImagem" class="btn btn-outline-danger btn-sm">Excluir</button>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Nome</label>
                        <input type="text" name="nome" class="form-control">
                      </div>
                      <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" class="form-control">
                      </div>
                      <!--<div class="form-group">
                        <label>Horário de pausa</label>
                        <input type="time" name="horarioPausa" class="form-control">
                      </div>-->
                    </div>
                  </div>
                </div>
              </div>
              <!--<div class="col-md-6 ">
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Membro cria sua senha?</label>
                          <button type="button" class="btn btn-toggle <?php //if($PaginasLista[0]->compartilhamento){echo 'active';};?>" data-toggle="button" aria-pressed="<?php //if($PaginasLista[0]->compartilhamento){echo 'true';}else{echo 'false';};?>" autocomplete="off">
                          <div class="handle"></div>
                          <input type="checkbox" name="compartilhamento" value="1" <?php //if($PaginasLista[0]->compartilhamento){echo 'checked';};?> hidden>
                        </button>
                      </div>
                      <div class="form-group">
                        <label>Senha</label>
                        <div class="input-group">
                          <input type="password" class="form-control input-senha">
                          <div class="input-group-append">
                            <span class="input-group-text olhar-senha"><i class="mdi mdi-eye-off icon-md"></i></span>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label>Repita a senha</label>
                        <div class="input-group">
                          <input type="password" class="form-control input-senha">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>-->
            </div>
                  
        </div>
      </div>

      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title">Serviços</h4>
          <p class="card-description">
            Selecione os serviços que o membro efetua.
          </p>
            <div class="row">
              <div class="col-md-12 ">
                  <div class="row">
                    <?php foreach ($categorias as $key => $categorias) {?>
                    <div class="col-md-4">
                      <label><?php echo $categorias;?></label>
                      <div class="checkboxes-wrap">
                        <div class="checkboxes-options">
                          <a href="#" class="check-all btn btn-outline-success btn-fw btn-sm">Todos</a> | <a href="#" class="check-none btn btn-outline-warning btn-fw btn-sm">Remover todos</a>
                        </div>
                        <div class="checkboxes">
                        <?php $servico = count($servicoLista);?>
                        <?php if($servico == 0){?>
                          <p>Nenhum serviço registrado</p>
                          <a class="btn btn-primary btn-rounded btn-sm" href="<?php echo URL;?>servicohomem">Adicionar</a>
                        <?php };?>
                        <?php foreach ($servicoLista as $linha) {?>
                          <?php if($linha->categoria == $categorias){ ?>
                          <div class="form-check form-check-success">
                            <label class="form-check-label">
                              <input type="checkbox" class="form-check-input" name="<?php echo 'servico'.$key.'[]';?>" value="<?php echo $linha->id;?>"><?php echo $linha->nome_servico;?>
                            </label>
                          </div>
                          <?php };?>
                        <?php };?>
                        </div>
                      </div>
                    </div>
                    <?php };?>
                  </div>
              </div>
            </div>       
        </div>
      </div>
      
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title">Horários</h4>
          <p class="card-description">
            Selecione os serviços que o membro efetua.
          </p>
          <form class="forms-sample" id="formularioPagina" action="<?php echo URL; ?>paginas/atualizar" method="POST">
            <div class="row">
              <div class="col-md-6 "> 
                <label>Segunda-feiras</label>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">      
                          <label>Das</label>
                          <input type="time" name="abertura" class="form-control" value="00:00:00">
                      </div>
                    </div>
                    <div class="col-md-4">
                    <div class="form-group">      
                      <label>Até</label>
                      <input type="time" name="fechamento" class="form-control" value="00:00:00">
                    </div>
                  </div>
                </div> 
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 "> 
                <label>Terças-feiras</label>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">      
                          <label>Das</label>
                          <input type="time" name="abertura" class="form-control" value="00:00:00">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">      
                        <label>Até</label>
                        <input type="time" name="fechamento" class="form-control" value="00:00:00">
                      </div>
                  </div>
                </div> 
              </div>
            </div>
          </form>         
        </div>
      </div>
      </form> 
    </div>
    
    <div class="col-md-3 grid-margin">
      <div class="card menu-atualizar">
        <div class="card-body">
          <h4 class="card-title">Menu membro</h4>
            <div class="media">
              <i class="mdi mdi-backup-restore icon-md d-flex align-self-start mr-3"></i>
              <div class="media-body d-flex align-self-center">
                <p class="card-text">Atualizado </p>
              </div>
            </div>
            <div class="media">
              <span class="d-flex align-self-start mr-3">
                <img src="<?php echo URL;?>images/faces/face4.jpg" alt="profile" style="border-radius: 100%;border: 2px solid #ececec;width: 30px;">
              </span>
              <div class="media-body d-flex align-self-center">
                  <p class="card-text">Editado por </p>
              </div>
            </div>
            <button destino="equipe/inserir" class="novo-ajax btn btn-inverse-success btn-rounded btn-fw">
              Adicionar
            </button>
          </div>
        </div>
      </div>