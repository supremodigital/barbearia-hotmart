<?php

namespace App\Model;

use App\Core\Model;

class NiveisUsuario extends Model
{

    public function lista()
    {
        $sql = "SELECT * FROM `niveis` WHERE 1";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function atualizar($id_niveisUsuario, $nivel)
    {
        $sql = "UPDATE niveis_usuario SET nome = :nome, email = :email, data_nasc = :data_nasc, cpf = :cpf WHERE id = :cliente_id";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome' => $nome, ':email' => $email, ':data_nasc' => $data_nasc, 'cpf' => $cpf, ':cliente_id' => $id_niveisUsuario);

        $query->execute($parameters);
    }

    public function adicionar($nivel)
    {
        $sql = "INSERT INTO niveis_usuario (nome, email, data_nasc, cpf) VALUES (:nome, :email, :data_nasc, :cpf)";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome' => $nome, ':email' => $email, ':data_nasc' => $data_nasc, ':cpf' => $cpf);

        $query->execute($parameters);
    }

    public function deletar($id_niveisUsuario)
    {
        $sql = "DELETE FROM niveis_usuario WHERE id = :id_niveisUsuario";
        $query = $this->db->prepare($sql);
        $parameters = array(':cliente_id' => $id_niveisUsuario);

        $query->execute($parameters);
    }

}
