<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Novo usuário adicionado com sucesso. </p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title">Novo usuário</h4>
          <span class="ajuda" data-tip="texto." tabindex="1"><i class="mdi mdi-help "></i></span>
          <p class="card-description">
            Adicione profissionais a sua equipe.
          </p>
          <form id="formularios">
            <div class="row">
            <div class="col-md-6 ">
                <div class="form-group">
                  <label>Informações do Perfil</label>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Nome</label>
                        <input type="text" name="nome" class="form-control" >
                      </div>
                      <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" class="form-control">
                      </div>
                      <div class="form-group">
                        <label>Nível</label>
                        <select name="nivel" class="form-control">
                        <?php foreach ($nivelListaa as  $linha) {?>
                          <option value="<?php echo $linha->id;?>"<?php if($linha->nivel == "cabeleireiro"){echo 'selected';}?>><?php echo $linha->nivel;?></option>
                        <?php } ?>
                        </select>
                      </div>
 
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row" style="justify-content: flex-end;">
            <button destino="usuario/criarUsuario" class="novo-ajax btn btn-inverse-success btn-rounded btn-fw">
              Adicionar
            </button>
            </div>
          </form>         
        </div>
      </div>    
    </div>   
  </div> 
</div>