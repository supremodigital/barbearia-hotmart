<?php

namespace App\Controller;

class AdminController
{
    public function index()
    {
        require APP . 'view/admin/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/admin/index.php';
        require APP . 'view/templates/footer.php';
    }
}
