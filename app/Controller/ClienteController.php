<?php
namespace App\Controller;

use App\Controller\EmailController;
use App\Model\Cliente;
use App\Model\Usuario;



class ClienteController
{
    private $nome;
    private $email;
    private $senha;

    public function __construct()
    {
        //(new LoginController)->usuarioLongado();
      
        //$nivelAcesso = new NivelController();
        //$nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
    }

    public function index()
    {
        $Cliente = new Cliente();
        $listaCliente = $Cliente->listaTodos();
		
		$tagTitle = "Lista de Cliente";
		$tagDescricao = "Painel Cliente";
		
        require APP . 'view/templates/header.php';
        require APP . 'view/cliente/index.php';
        require APP . 'view/templates/modal.php';
        require APP . 'view/templates/footer.php';
    }

    public function consultaEmail()
    {
        $nome = $_POST['cliEmail'];

        if(!empty($cliEmail))
        {
            $ClienteEmail = new Cliente();
            $consultaEmail = $ClienteEmail->consultaEmail($cliEmail);

            if (!empty($consultaEmail)) {
                foreach ($consultaEmail as $linha) {

                    
                }
            } else {
                
            } 
        } 
    }

    public function novo()
    {
        require APP . 'view/templates/header.php';
        require APP . 'view/cliente/novo.php';
        require APP . 'view/templates/footer.php';
    }

    public function editar($id)
    {
        $Cliente 		= new Cliente();
        $listaCliente 	= $Cliente->lista($id);

        require APP . 'view/templates/header.php';
        require APP . 'view/cliente/editar.php';
        require APP . 'view/templates/footer.php';
    }

    public function desativar($id)
    {
        $Cliente 		= new Cliente();
        $cliente 		= $Cliente->desativar($id);
        echo json_decode($cliente);

    }

    public function atualizar($id)
    {

        $imagem = $_FILES['imagem'];

        $Cliente = new Cliente();
        $listaCliente = $Cliente->lista($id);

        $imgBanco = $cliente[0]->img;
        $handle = new \Verot\Upload\Upload($imagem);
        $imgInput = $handle->file_src_name;
    
        if (!empty($imgInput)) { //Se tiver imagem input
            
            $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/cliente');    
            $handle = new \Verot\Upload\Upload($imagem);
    
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 100;
                $handle->image_ratio_y        = true;
                $handle->file_safe_name = false;
                $handle->file_name_body_add = '_mini';
                $handle->process($diretorio_destino);
        
            }
        
            if ($handle->uploaded)
            {
                $handle->image_resize         = true;
                $handle->image_x              = 680;
                $handle->image_ratio_y        = true;
                $handle->process($diretorio_destino);
        
            }

            $img = $handle->file_src_name;
            
        } else {
            $img = $imgBanco;
        } 
        
        $msgModal = $Cliente->atualizar($id, 
										$_POST['cliNome'],
										$_POST['cliProfissao'],
										$_POST['cliEmail'],
										$_POST['cliSenha'],
										$_POST['cliRecuperar'],
										$_POST['cliFoneUm'],
										$_POST['cliFoneDois'],
										$_POST['cliStatus'],
										$img );
        echo json_encode($msgModal);    

    }

    public function inserir()
    {
        $Cliente = new Cliente();
		
		$imagem = $_FILES['imagem'];

        $diretorio_destino = (isset($_GET['dir']) ? $_GET['dir'] : 'images/cliente');    
        $handle = new \Verot\Upload\Upload($imagem);

        if ($handle->uploaded)
        {
            $handle->image_resize         = true;
            $handle->image_x              = 100;
            $handle->image_ratio_y        = true;
            $handle->file_safe_name = false;
            $handle->file_name_body_add = '_mini';
            $handle->process($diretorio_destino);

        }

        if ($handle->uploaded)
        {
            $handle->image_resize         = true;
            $handle->image_x              = 680;
            $handle->image_ratio_y        = true;
            $handle->process($diretorio_destino);

        }

        $img = $handle->file_src_name;

        
        $msgModal = $Cliente->inserir($_POST['cliNome'],
										$_POST['cliProfissao'],
										$_POST['cliEmail'],
										$_POST['cliSenha'],
										$_POST['cliRecuperar'],
										$img,
										$_POST['cliFoneUm'],
										$_POST['cliFoneDois'],
										$_POST['cliStatus']);
 
        echo json_encode($msgModal);
    }

}

