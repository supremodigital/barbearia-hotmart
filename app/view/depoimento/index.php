<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-atualizar">Depoimento aceito com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium texto-ajax-erro">Depoimento rejeitado.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <?php if (!empty($depoimentoLista)) {?>
      <div class="card grid-margin">
        <div id="corpo-card" class="card-body">
          <h4 class="card-title">Recebeu um novo depoimento</h4>
          <?php foreach ($depoimentoLista as $novodepo) {?>
          <div class="table-responsive pt-3 novo-depoimento-<?php echo $novodepo->idDepoimento;?>">
            <table class="table table-bordered">
              <thead style=" background: #4d83ff;border: none;color: white;">
                <tr>
                  <th>Perfil</th>
                  <th>Nome</th>
                  <th>Profissão</th>
                  <th>Comentário</th>
                  <th></th>  
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <img src="<?php echo URL.'images/cliente/'.$novodepo->img;?>" alt="image">
                  </td>
                  <td>
                  <?php echo $novodepo->nome_cliente;?>
                  </td>
                  <td>
                  <?php echo $novodepo->profissao;?>
                  </td>
                  <td>
                  <?php echo mb_strimwidth($novodepo->depoimento, 0, 30, "...");?>
                  </td>
                  <td>
                    <button type="button" title="Aceitar Depoimento" destino="depoimento/atualizarEstatus/aprovado" idobjeto="<?php echo $novodepo->id;?>" class="depoimento-ajax btn btn-outline-secondary btn-rounded btn-icon">
                      <i class="mdi mdi-check text-primary"></i>
                    </button>
                    <button type="button" title="Rejeitar Depoimento" destino="depoimento/atualizarEstatus/rejeitado"  idobjeto="<?php echo $novodepo->id;?>" class="depoimento-ajax btn btn-outline-secondary btn-rounded btn-icon">
                      <i class="mdi mdi-close text-primary"></i>
                    </button>
                  </td>
                </tr>       
              </tbody>
            </table>
          </div>
          <?php }?>
        </div>
      </div>
      <?php }?>
      <div class="card grid-margin">
        <div id="corpo-card" class="card-body">
          <h4 class="card-title">Depoimento</h4>
          <span class="ajuda" data-tip="texto." tabindex="1"><i class="mdi mdi-help "></i></span>
          <a href="depoimento/novo" class="btn btn-inverse-secondary btn-rounded btn-fw" style="float:right">
            Novo
          </a>
          <p class="card-description">
            Conteúdo de destaque do site com seu serviço ou produto.
          </p>
          <table class="table table-responsive table-striped">
            <thead>
              <tr>
                <th>Perfil</th>
                <th>Nome</th>
                <th>Profissão</th>
                <th>Comentário</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($depoimentoLista as $linha) { ?>
              <tr id="linha<?php echo $linha->idDepoimento; ?>">
                <td class="py-1">
                  <img src="<?php echo URL . 'images/depoimento/'?><?php $img = explode('.',$linha->img); echo $img[0] . "_mini." . $img[1]; ?>" alt="image">
                </td>
                <td>
                  <?php echo $linha->depoimentoDescricao; ?>
                </td>
                <td>
                  <?php echo $linha->depoimentoAvaliacao; ?>
                </td>
                <td>
                <?php echo mb_strimwidth($linha->depoimentoAvaliacao, 0, 30, "..."); ?>
                </td>
                <td>
                <a title="Editar" href="depoimento/editar/<?php echo $linha->idDepoimento; ?>" class="btn btn-inverse-primary btn-rounded btn-icon">
                  <div class="centro-icone">
                    <i class="mdi mdi-lead-pencil"></i>
                  </div>
                </a>
                  <button type="button" title="Deletar" class="deletar-modal btn btn-inverse-danger btn-rounded btn-icon"  idobjeto="<?php echo $linha->idDepoimento; ?>" msg="depoimento do cliente <?php echo $linha->nome; ?>" destino="depoimento/deletar/" data-toggle="modal" data-target="#deletarModal">
                    <i class="mdi mdi-delete-forever"></i>
                  </button>
                </td>
              </tr>
            <?php } ?>
            </tbody>
          </table>
        </div>
      </div>     
    </div> 
  </div>
</div>