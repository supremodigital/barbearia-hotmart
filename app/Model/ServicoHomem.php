<?php

namespace App\Model;

use App\Core\Model;

class ServicoHomem extends Model
{

    public function listaTodos()
    {
        $sql = "SELECT servico_homem.*,categoria_servico.categoria FROM `servico_homem` INNER JOIN categoria_servico ON categoria_servico.id=servico_homem.id_categoria WHERE 1 ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaServicos()
    {
        $sql = "SELECT nome_servico FROM `servico_homem` WHERE 1";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function lista($id)
    {
        $sql = "SELECT servico_homem.*,categoria_servico.categoria  FROM `servico_homem` INNER JOIN categoria_servico ON categoria_servico.id=servico_homem.id_categoria WHERE servico_homem.id=$id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function reservaIndex()
    {
        $sql = "SELECT servico_homem.id,servico_homem.nome_servico,servico_homem.valor,categoria_servico.categoria FROM `servico_homem` INNER JOIN categoria_servico ON categoria_servico.id=servico_homem.id_categoria WHERE 1 ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function TodosIdNome()
    {

        $sql = "SELECT id, nome_servico, valor FROM servico_homem WHERE 1";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function TodosIdNomeCategorias()
    {
        $sql = "SELECT servico_homem.id, servico_homem.nome_servico, categoria_servico.categoria FROM `servico_homem` INNER JOIN categoria_servico ON categoria_servico.id=servico_homem.id_categoria WHERE 1";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function idNomeCategorias($id)
    {
        $sql = "SELECT servico_homem.id, servico_homem.nome_servico, categoria_servico.categoria FROM `servico_homem` INNER JOIN categoria_servico ON categoria_servico.id=servico_homem.id_categoria WHERE 1";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function TodosIdNomeCategoria()
    {
        $sql = "SELECT servico_homem.id, servico_homem.nome_servico, categoria_servico.categoria, servico_homem.valor FROM `servico_homem` INNER JOIN categoria_servico ON categoria_servico.id=servico_homem.id_categoria WHERE 1";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function consultaCategoria($categoria)
    {
        $sql = "SELECT id FROM `servico_homem` WHERE id_categoria='$categoria'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function atualizar($id, $nomeServico, $descricao, $categoria, $valor, $tempo, $img, $alt )
    {
        $sql = "update servico_homem set nome_servico = '".$nomeServico."', descricao = '".$descricao."',id_categoria = '".$categoria."',valor = '".$valor."',tempo = '".$tempo."',img = '".$img."',alt = '".$alt."' where id = ".$id;
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    
    }

    public function inserir($nomeServico, $descricao, $categoria, $valor, $tempo, $img, $alt )
    {
        $sql = "INSERT INTO servico_homem (nome_servico, descricao, id_categoria, valor, tempo, img, alt) VALUES (:nome_servico, :descricao, :id_categoria, :valor, :tempo, :img, :alt)";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome_servico' => $nomeServico, ':descricao' => $descricao, ':id_categoria' => $categoria, ':valor' => $valor, ':tempo' => $tempo, ':img' => $img, 'alt' => $alt);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function deletar($id)
    {
        $sql = "DELETE FROM servico_homem WHERE id = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

}
