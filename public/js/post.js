
var url = document.location.origin;
if (url == "https://goldtimegestao.com.br") {
  var url = document.location.origin + "/goldtime";
} else{
  var url = document.location.origin + "/barbearia-hotmart";
}

//==================== NOVO DOCUMENTO =======================

$(".novo-ajax").click(function () {
    var vazio = $("input[name=titulo]").filter(function () { return !this.value; }).get();

    var destino = $(this).attr('destino');
    var evento = this;

    if (vazio.length) {

        $('#erro-atualizar').toggleClass('d-none');

    } else {

        $(evento).prop('disabled', true);
        $('#erro-atualizar').addClass('d-none');

        $.ajax({

            type: "POST",
            data: new FormData($('#formularios')[0]),
            cache: false,
            contentType: false,
            processData: false,
            url: url + "/" + destino,
            dataType: "html",
            success: function (retorno) {
                $('.card-title').html(retorno);
                var msgModal = JSON.parse(retorno);
                if (msgModal) {
                    $('#atualizado-sucesso').removeClass('d-none');
                    $(evento).prop('disabled', false);
                } else {
                    $('#erro-atualizar').toggleClass('d-none');
                    $(evento).prop('disabled', false);
                }
            },
            beforeSend: function () {

            },
            complete: function (msg) {

            }
        });
    }

});

//==================== F =======================

$("#formularios .btn-toggle").click(function () {
    $('#atualizado-sucesso').addClass('d-none');
    $('#erro-atualizar').addClass('d-none');
    $('.novo-ajax').prop('disabled', false);
    $('.link-ajax-atualizar').prop('disabled', false);
    $('.perfil-atualizar').prop('disabled', false);
});
$("#formularios input").keydown(function () {
    $('#atualizado-sucesso').addClass('d-none');
    $('#erro-atualizar').addClass('d-none');
    $('.novo-ajax').prop('disabled', false);
    $('.link-ajax-atualizar').prop('disabled', false);
    $('.perfil-atualizar').prop('disabled', false);
});
$("#formularios textarea").keydown(function () {
    $('#atualizado-sucesso').addClass('d-none');
    $('#erro-atualizar').addClass('d-none');
    $('.novo-ajax').prop('disabled', false);
    $('.link-ajax-atualizar').prop('disabled', false);
    $('.perfil-atualizar').prop('disabled', false);
});

$('.link-ajax-atualizar').on('click', function () {

    var evento = this;
    var destino = $(this).attr('destino');
    $(evento).prop('disabled', true);

    $.ajax({

        type: "POST",
        data: new FormData($('#formularios')[0]),
        cache: false,
        contentType: false,
        processData: false,
        url: url + "/" + destino,
        dataType: "html",
        success: function (retorno) {
            //$('.card-title').html(retorno);
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                $('#atualizado-sucesso').removeClass('d-none');
            } else {
                $('#erro-atualizar').toggleClass('d-none');
            }
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });
});

//================ DEPOIMENTO ====================
$(".depoimento-ajax").click(function () {
    var evento = this;

    var destino = $(this).attr('destino');
    var id = $(this).attr('idobjeto');

    var depoimento = $('.novo-depoimento-' + id);

    var fd = new FormData();
    fd.append('id', id);

    $.ajax({

        type: "POST",
        data: fd,
        url: url + '/' + destino ,
        dataType: "html",
        cache: false,
        contentType: false,
        processData: false,
        success: function (retorno) {
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                $('#atualizado-sucesso').removeClass('d-none');
                depoimento.hide("slow");
            } else {
                $('#erro-atualizar').toggleClass('d-none');
            }
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });
});

$('.link-ajax').on('click', function () {

    var link = $(this).attr('link');
    $('.spinner').css({ display: "block" });

    $.ajax(url + "/paginas/" + link)
        .done(function (result) {
            $('#main').html(result);
        })
        .fail(function () {

        })
        .always(function () {
            $('.spinner').hide("slow");
        });
});

$("#atualizarPagina").click(function () {

    var fd = new FormData();
    fd.append('imagem', $('#inputImagem'));
    fd.append('titulo', $("input[name=nome_cliente]").val());

    $.ajax({

        type: "POST",
        data: fd,
        url: url + "/paginas/atualizar",
        dataType: "html",
        success: function (result) {
            alert('atualizado');
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });
});

$("#atualizarPaginaa").click(function () {
    var evento = this;

    //$('.carregar-pagina').css({ display: "flex" });

    $('#atualizado-sucesso').toggleClass('d-none');

    $.ajax({
        url: url + "/paginas/atualizar",
        method: 'post',
        data: new FormData($('#myForm')[0]),
        cache: false,
        contentType: false,
        processData: false,
        success: function (retorno) {
            //$('.carregar-pagina').hide("slow");
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                $('#atualizado-sucesso').removeClass('d-none');
            } else {
                $('#erro-atualizar').toggleClass('d-none');
            }
        },
        beforeSend: function () {
            $(evento).prop('disabled', true);
        },
        complete: function (msg) {
            $(evento).prop('disabled', false);
        }
    });// Fim do ajax

});


// CONTATO JSON

$(".ajaxcontato").click(function () {
    var evento = this;
    var id = $(this).attr('destino');

    $.ajax({
        url: url + "/contato/contatoJson",
        method: 'post',
        dataType: "html",
        data: {
            id: id,
        },
        success: function (retorno) {
            objeto = JSON.parse(retorno);
            //$('#teste').html(objeto);
            var msgModal = JSON.parse(retorno);
            $('#contatoNome').val(objeto[0][0]["nome"]);
            $('#contatoAssunto').val(objeto[0][0]["assunto"]);
            $('#contatoTel').val(objeto[0][0]["telefone"]);
            $('#contatoData').val(objeto[0][0]["data"]);
            $('#contatoEmail').val(objeto[0][0]["email"]);
            $('#contatoMsg').val(objeto[0][0]["mensagem"]);
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });
});
$(".ajaxEmail").click(function () {
    var email = $(this).attr('destino');
    var assunto = $(this).attr('assunto');
    $('.contatoEmail').val(email);
    $('.contatoAssunto').val(assunto);

});
$("#enviarEmail").click(function () {

    $.ajax({

        type: "POST",
        data: new FormData($('#formulariosContato')[0]),
        cache: false,
        contentType: false,
        processData: false,
        url: url + "/contato/enviar",
        dataType: "html",
        success: function (retorno) {
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                $('#atualizado-sucesso').removeClass('d-none');
            } else {
                $('#erro-atualizar').toggleClass('d-none');
            }
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
        });
    });

$(".ajaxApagarContato").click(function () {

    var id = $(this).attr('destino');

        $.ajax({

            url: url + "/contato/deletar",
            method: 'post',
            dataType: "html",
            data: {
                id: id,
            },
            success: function (retorno) {
                var msgModal = JSON.parse(retorno);
                if (msgModal) {
                    $('#atualizado-sucesso').removeClass('d-none');
                } else {
                    $('#erro-atualizar').toggleClass('d-none');
                }
            },
            beforeSend: function () {

            },
            complete: function (msg) {

            }
        });
    });

// NOTIFICACAO JSON


var tid = setInterval(notificar, 8000);
function notificar() {

    var quantidadeNortificacao = $('#notificacao-ajax').attr('qtd');

    $.ajax({
        url: url + "/notificacao/verificar",
        method: 'post',
        dataType: "json",
        data: {
            qtd: quantidadeNortificacao,
        },
        success: function (retorno) {
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                ativarnotificacao();
            }
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });
};
function ativarnotificacao() {

    var quantidadeNortificacao = $('#notificacao-ajax').attr('qtd');

    $.ajax({
        url: url + "/notificacao/certo",
        method: 'post',
        dataType: "html",
        data: {
            qtd: 1,
        },
        success: function (retorno) {
            $('#audio')[0].play();
            $('.count').text('1');
            $('.count').css('display', 'initial');
            $('#notificacao-ajax').html(retorno);
            $('#notificacao-ajax').attr('qtd', ++quantidadeNortificacao);
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });
};

// CARREGAR FILA
function carregarFila() {

    $.ajax({
        url: url + "/reserva/carregarFila",
        method: 'post',
        dataType: "html",

        success: function (retorno) {
            $('.ajax-carregamento').html(retorno);
        },
        beforeSend: function () {
            $('.ajax-carregamento').addClass('carregar');
        },
        complete: function (msg) {
            $('.ajax-carregamento').removeClass('carregar');
        }
    });

}
function carregarFilaTempo(dataa) {

    $.ajax({
        url: url + "/reservatempo/carregarFila",
        method: 'post',
        dataType: "html",
        data: {
            dataa: dataa,
        },

        success: function (retorno) {
            $('.ajax-carregamento').html(retorno);
        },
        beforeSend: function () {
            $('.ajax-carregamento').addClass('carregar');
        },
        complete: function (msg) {
            $('.ajax-carregamento').removeClass('carregar');
        }
    });

}
function carregarFilaConsulta(data) {

    $.ajax({
        url: url + "/reservatempo/carregarFilaConsulta",
        method: 'post',
        dataType: "html",
        data: {
            dataa: data,
        },

        success: function (retorno) {
            $('.ajax-carregamento').html(retorno);
        },
        beforeSend: function () {
            $('.ajax-carregamento').addClass('carregar');
        },
        complete: function (msg) {
            $('.ajax-carregamento').removeClass('carregar');
        }
    });

}
function carregarHorarios(dataa) {

    $.ajax({
        url: url + "/reservatempo/carregarhorario",
        method: 'post',
        dataType: "html",
        data: {
            data: dataa,
        },

        success: function (retorno) {
            $('#horarioDisponivel').html(retorno);
        },
        beforeSend: function () {
            $('.ajax-carregamento').addClass('carregar');
        },
        complete: function (msg) {
            $('.ajax-carregamento').removeClass('carregar');
        }
    });

}

$("#consultar-nome").click(function () {
    consultarnome();
    $("#lista-nome").show("slow");
});
$("input[name=nome-pesquisa]").bind("enterKey", function (e) {
    consultarnome();
    $("#lista-nome").show("slow");
});
$('input[name=nome-pesquisa]').keyup(function (e) {
    if (e.keyCode == 13) {
        $(this).trigger("enterKey");
    }
});


$("#lista-nome").click(function () {

    $nome = $('input[name=nomeselecionado]:checked').val();
    $id = $('input[name=nomeselecionado]:checked').attr('id');
    $("input[name=nome-pesquisa]").val($nome);
    $("input[name=id]").val($id);
    $(this).hide("slow");

});

// ATIVAR INPUT NOVO CLIENTE

var ativo = true;
$(".novo-cliente").click(function (e) {
    e.preventDefault();
    if (ativo) {
        $(".novo-input").show("slow");
        $(".pesquisa-input").hide();
        ativo = !ativo;
    }
    else {
        $(".pesquisa-input").show("slow");
        $(".novo-input").hide("slow");
        ativo = !ativo;
    }

});
// Registrar Reserva
$("#reserva-chegada-cliente").click(function () {

    //VALIDACAO
    var profissionalvazio = $("input[name=profissional]").filter(function () { return !this.value; }).get();
    var servico = $('input[name="servico[]"]:checked').length > 0;
    
    if (!profissionalvazio.length && servico) {

        var evento = this;

        $.ajax({
            url: url + "/painelcliente/fazerReserva",
            method: 'post',
            data: new FormData($('#formularios')[0]),
            cache: false,
            contentType: false,
            processData: false,
            dataType: "html",
            success: function (retorno) {
                var msgModal = JSON.parse(retorno);
                if (msgModal) {
                    $('#atualizado-sucesso').removeClass('d-none');
                    carregarFila();
                } else {
                    $('#erro-atualizar').toggleClass('d-none');
                }
            },
            beforeSend: function () {
                $(evento).prop('disabled', true);
            },
            complete: function (msg) {
                $(evento).prop('disabled', false);
            }
        });// Fim do ajax

    } else {

        $('#erro-atualizar .texto-ajax-erro').text('Nome do Cliente obrigatório');
        $('#erro-atualizar').toggleClass('d-none');
       
    }
});

$("#reservachegada").click(function () {
    //VALIDACAO
    //var nomevazio = $("input[name=nome]").filter(function () { return !this.value; }).get();
    var pesquisavazio = $("input[name=nome-pesquisa]").filter(function () { return !this.value; }).get();
    var servico = $('input[name="servico[]"]:checked').length > 0;
    
    if (true) {

        var evento = this;

        $.ajax({
            url: url + "/reserva/analise",
            method: 'post',
            data: new FormData($('#formularios')[0]),
            cache: false,
            contentType: false,
            processData: false,
            dataType: "html",
            success: function (retorno) {
                $('.msg').html(retorno);
                var msgModal = JSON.parse(retorno);
                if (msgModal) {
                    $('#atualizado-sucesso').removeClass('d-none');
                    carregarFila();
                } else {
                    $('#erro-atualizar').toggleClass('d-none');
                }
            },
            beforeSend: function () {
                $(evento).prop('disabled', true);
            },
            complete: function (msg) {
                $(evento).prop('disabled', false);
            }
        });// Fim do ajax

    } else {

        //$('#erro-atualizar .texto-ajax-erro').text('Nome do Cliente obrigatório');
        //$('#erro-atualizar').toggleClass('d-none');
       
    }
});

$("#reservaTempo").click(function () {

    data = $('#data').val();

    //VALIDACAO
    var nomevazio = $("input[name=nome]").filter(function () { return !this.value; }).get();
    var pesquisavazio = $("input[name=nome-pesquisa]").filter(function () { return !this.value; }).get();
    var servico = $('input[name="servico[]"]:checked').length > 0;
    
    if (!nomevazio.length && servico || !pesquisavazio.length & servico ) {

        var evento = this;

        $.ajax({
            url: url + "/reservatempo/analise",
            method: 'post',
            data: new FormData($('#formularios')[0]),
            cache: false,
            contentType: false,
            processData: false,
            dataType: "html",
            success: function (retorno) {
                
                $('.card-description').html(retorno);
                var msgModal = JSON.parse(retorno);
                
                if (msgModal["boleano"]) {
                    $('#atualizado-sucesso').removeClass('d-none');
                    //$('#nome').val(msgModal["data"]);
                    carregarFilaTempo(msgModal["data"]);
                } else {
                    $('#erro-atualizar').toggleClass('d-none');
                }
            },
            beforeSend: function () {
                $(evento).prop('disabled', true);
            },
            complete: function (msg) {
                $(evento).prop('disabled', false);
            }
        });// Fim do ajax

        

    } else if (!servico) {
        $('#erro-atualizar .texto-ajax-erro').text('Escolha do serviço é obrigatório');
        $(".menu-servico li label").css({'border-color': '#4d83ff'});
        $('#erro-atualizar').toggleClass('d-none');
        
    } else {
        $('#erro-atualizar .texto-ajax-erro').text('Nome do Cliente obrigatório');
        $("input[name=nome]").css({'border-color': '#4d83ff'});
        $("input[name=nome-pesquisa]").css({'border-color': '#4d83ff'});
        $('#erro-atualizar').toggleClass('d-none');
        
    }

});

$("#confirmarData").click(function () {
    var dataa = $('#dataSelecionada').text();
    $('#data').val(dataa);
    $('.date-picker').removeClass('open');
    carregarFilaConsulta(dataa);
    carregarHorarios(dataa);
});

// LISTA NOMES
function consultarnome() {
    var nome = $("input[name=nome-pesquisa]").val();

    $.ajax({
        url: url + "/cliente/consultaNome",
        method: 'post',
        dataType: "html",
        data: {
            nome: nome,
        },
        success: function (retorno) {
            $('#lista-nome').html(retorno);
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });

}
// FORMULARIO LOGIN
$("#nova-senha-cliente").click(function () {
    event.preventDefault();
    var evento = this;

    var form = $('#form-nova-senha-cliente');
    $(form).find('.mensagem').hide();
  
    var senha = $("input[name=nova-senha]").val();
    var repita_senha = $("input[name=repita-senha]").val();
    
    if (senha == "") {
        $(form).find('.mensagem').show('slow');
        $(form).find('.mensagem').addClass('erro');
        $(form).find('.mensagem').text('Digite a nova senha')
    } else if (senha == repita_senha) {
      
  
        $.ajax({
          url: url + "/login/salvarSenhaCliente",
          method: 'post',
          data: new FormData($('#form-nova-senha-cliente')[0]),
          cache: false,
          contentType: false,
          processData: false,
          dataType: "html",
          success: function (retorno) {
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
              $(form).find('.mensagem').show('slow');
              $(form).find('.mensagem').addClass('sucesso');
              $(form).find('.mensagem').text('Senha salva com sucesso');
              setTimeout(function(){ window.location.href = '../../painelcliente'; }, 2500);
            }
          },
          beforeSend: function () {
  
          },
          complete: function (msg) {
  
          }
        });
      } else{
        $(form).find('.mensagem').show('slow');
        $(form).find('.mensagem').addClass('erro');
        $(form).find('.mensagem').text('Senhas não são iguais')
      }
  });

$("#botaoLogin").click(function () {
    var evento = this;

    $.ajax({
        url: url + "/login/acesso",
        method: 'post',
        data: new FormData($('#formLogin')[0]),
        cache: false,
        contentType: false,
        processData: false,
        success: function (retorno) {
            var msgModal = JSON.parse(retorno);
            console.log(msgModal);
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });// Fim do ajax

});

$("#botaoLoginn").click(function () {

    var senha = $('input[name=senha]').val();
    var nome_email = $("input[name=nome_email]").val();

    $.ajax({

        type: "POST",
        data: {
            senha: senha,
            nome_email: email,
            grecaptcharesponse: grecaptcha.getResponse()
        },
        url: url + "/login/acesso",
        dataType: "html",
        success: function (result) {
            $('#teste').html(result);
            var msgModal = JSON.parse(result);
            console.log(msgModal);
            alert(msgModal);
            grecaptcha.reset();
        },
        beforeSend: function () {

        },
        complete: function (msg) {
            var msgModal = JSON.parse(msg);
            console.log(msgModal);
            alert(msgModal);
        }
    });
});


var contactForm = $("#formLoginn");
contactForm.on("submit", function (e) {

    e.preventDefault();

    var nome_email = $("input[name=nome_email]").val();
    var senha = $('input[name=senha]').val();

    $.ajax({
        type: "POST",
        url: url + "/login/teste", //Our file 
        data: {
            nome_email: nome_email,
            senha: senha,
            captcha: grecaptcha.getResponse()

        },
        success: function (response) {
            $('#teste').html(response);


            // grecaptcha.reset(); // Reset reCaptcha
        }
    })
});

$('.deletar-modal').on('click', function () {
    $('#atualizado-sucesso').addClass('d-none');
});

$('.link-ajaxxx').on('click', function () {

    var link = $(this).attr('destino');
    var id = $(this).attr('idobjeto');

    $(".btn-inverse-danger").trigger('click');

    $.ajax(url + "/" + link + id)
        .done(function (resultado) {
            var msgModal = JSON.parse(resultado);
            console.log(msgModal);
            if (msgModal) {
                var linha = $('#linha' + id);
                linha.hide('slow', function () { linha.remove(); });
                $('html, body').animate({ scrollTop: 0 }, 1000, 'linear');
                $('#atualizado-sucesso').removeClass('d-none');
                $('#atualizado-sucesso .texto-ajax-atualizar').text('Deletado com sucesso');
            } else {
                $('#erro-atualizar').removeClass('d-none');
                $('#erro-atualizar .texto-ajax-erro').text('Erro ao deletar arquivo');
            }
        })
        .fail(function () {

        })
        .always(function () {

        });
});


$('.link-ajax-novo').on('click', function () {

    var link = $(this).attr('link');
    var evento = this;

    $.ajax({
        url: url + "/" + link,
        method: 'post',
        data: new FormData($('#formularios')[0]),
        dataType: "html",
        success: function (resultado) {
            var msgModal = JSON.parse(resultado);
            console.log(msgModal);
            if (msgModal) {
                $('#erro-atualizar').removeClass("d-none");
            }
            $(evento).attr("hidden", false);
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }

    });

});

// Atendimento
$(".iniciar-atendimento").on('click', function () {

    var evento = this;
    var id = $(this).attr('idobjeto');
    var destino = $(this).attr('destino');

    $.ajax({
        url: url + "/" + destino,
        method: 'post',
        dataType: "html",
        data: {
            id: id,
        },
        success: function (retorno) {
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                var linha = $('#estatus' + id);
                //linha.hide('slow', function () { linha.remove(); });
                $(evento).hide();
                $(".finalizado-atendimento").show();
                $('#atualizado-sucesso').removeClass('d-none');
                $(linha).html('<label class="badge badge-success">Atendimento</label>');
                $(".perdeu-atendimento-oculta").hide("slow");
            } else {
                $('#erro-atualizar').removeClass('d-none');
            }
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });
});
$(document).on("click",".iniciar-atendimento-carregado", function(){

    var id = $('.iniciar-atendimento-carregado').attr('idobjeto');
    var destino = $('.iniciar-atendimento-carregado').attr('destino');

    $.ajax({
        url: url + "/" + destino,
        method: 'post',
        dataType: "html",
        data: {
            id: id,
        },
        success: function (retorno) {
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                var linha = $('#estatus' + id);
                //linha.hide('slow', function () { linha.remove(); });
                $('#atualizado-sucesso .texto-ajax-atualizar').text('Iniciou o serviço com sucesso.');
                $('#atualizado-sucesso').removeClass('d-none');
                $(".perdeu-atendimento-oculta").hide("slow");
                $('.iniciar-atendimento-carregado').hide();
                $(".finalizado-atendimento").show();
                $(linha).html('<label class="badge badge-success">Atendimento</label>');
            } else {
                $('#erro-atualizar').removeClass('d-none');
            }
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });
});
$(document).on("click",".finalizar-atendimento-carregado", function(){

    var id = $(this).attr('idobjeto');
    var destino = $(this).attr('destino');

    $.ajax({
        url: url + "/" + destino,
        method: 'post',
        dataType: "html",
        data: {
            id: id,
        },
        success: function (retorno) {
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                var linha = $('#linha' + id);
                linha.hide('slow', function () { linha.remove(); });
                $('#atualizado-sucesso').removeClass('d-none');
                $('#atualizado-sucesso .texto-ajax-atualizar').text('Serviço finalizado com sucesso');
                setTimeout(function(){ $('#atualizado-sucesso').addClass('d-none'); }, 2300);
                carregarAtendimento(destino);
            } else {
                $('#erro-atualizar').removeClass('d-none');
            }
        },
        beforeSend: function () {
            
        },
        complete: function (msg) {

        }
    });
});
$(".finalizar-atendimento-ajax").on('click', function () {

    var id = $(this).attr('idobjeto');
    var destino = $(this).attr('destino');

    $.ajax({
        url: url + "/" + destino,
        method: 'post',
        dataType: "html",
        data: {
            id: id,
        },
        success: function (retorno) {
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                var linha = $('#linha' + id);
                linha.hide('slow', function () { linha.remove(); });
                $('#atualizado-sucesso').removeClass('d-none');
                $('#atualizado-sucesso .texto-ajax-atualizar').text('Serviço finalizado com sucesso');
                setTimeout(function(){ $('#atualizado-sucesso').addClass('d-none'); }, 2300);
                carregarAtendimento(destino);
            } else {
                $('#erro-atualizar').removeClass('d-none');
            }
        },
        beforeSend: function () {
            
        },
        complete: function (msg) {

        }
    });
})
$(".perdeu-atendimento").on('click', function () {

    var id = $(this).attr('idobjeto');
    var destino = $(this).attr('destino');

    $.ajax({
        url: url + "/" + destino,
        method: 'post',
        dataType: "html",
        data: {
            id: id,
        },
        success: function (retorno) {
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                var linha = $('#linha' + id);
                linha.hide('slow', function () { linha.remove(); });
                $('#alerta').removeClass('d-none');
                $('#alerta .texto-ajax-alerta').text('Serviço finalizado com sucesso');
                setTimeout(function(){ $('#alerta').addClass('d-none'); }, 2300);
                carregarAtendimento(destino);
            } else {
                $('#erro-atualizar').removeClass('d-none');
            }
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });
})
$(document).on("click",".perdeu-atendimento-carregado", function(){

    var id = $('.perdeu-atendimento-carregado').attr('idobjeto');
    var destino = $('.perdeu-atendimento-carregado').attr('destino');

    $.ajax({
        url: url + "/" + destino,
        method: 'post',
        dataType: "html",
        data: {
            id: id,
        },
        success: function (retorno) {
            var msgModal = JSON.parse(retorno);
            if (msgModal) {
                var linha = $('#linha' + id);
                linha.hide('slow', function () { linha.remove(); });
                $('#alerta').removeClass('d-none');
                $('#alerta .texto-ajax-alerta').text('Serviço finalizado com sucesso');
                setTimeout(function(){ $('#alerta').addClass('d-none'); }, 2300);
                carregarAtendimento(destino);
            } else {
                $('#erro-atualizar').removeClass('d-none');
            }
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });

});
function carregarAtendimento(destino) {

    if (destino == "reserva/finalizarAtendimento" || destino == "reserva/perdeuAtendimento") {
        var destinos = "reserva/carregarAtendimento"
    } else {
        var destinos = "reservaTempo/carregarAtendimento"
    }

    $.ajax({
        url: url + "/" + destinos,
        method: 'post',
        dataType: "html",

        success: function (retorno) {
            $('#lista-Atendimento').html(retorno);
        },
        beforeSend: function () {
            $('.ajax-carregamento').addClass('carregar');
        },
        complete: function (msg) {
            $('.ajax-carregamento').removeClass('carregar');
        }
    });

}

$('.iniciar-atendimento').on('click', function () {
    //$(this).removeClass('btn-primary');
    //$(this).addClass('btn-success');
    //$(this).text('Finalizado?');
    //$(this).attr( "data-toggle", "modal" );
    //$(this).attr( "data-target", "#FinalizadoModa" );
    //$(this).hide();
    //$(".finalizado-atendimento").show();
});



/*======== PERFIL =========*/
$('.perfil-atualizar').on('click', function () {

    var evento = this;
    var destino = $(this).attr('destino');
    $(evento).prop('disabled', true);

    $.ajax({

        type: "POST",
        data: new FormData($('#formularios')[0]),
        cache: false,
        contentType: false,
        processData: false,
        url: url + "/" + destino,
        dataType: "html",
        success: function (retorno) {
            //$('.msg').html(retorno);
            var msgModal = JSON.parse(retorno);
            if (msgModal['senha'] == 0) {
                $('#erro-atualizar .texto-ajax-erro').text('Senha antiga incorreta');
                $('#erro-atualizar').toggleClass('d-none');         
            }else if (msgModal['dados']) {
                $('#atualizado-sucesso').removeClass('d-none');
            } else {
                $('#erro-atualizar').toggleClass('d-none');
            }
        },
        beforeSend: function () {

        },
        complete: function (msg) {

        }
    });
});