<?php

namespace App\Controller;

use App\Controller\NivelController;
use App\Controller\EmailController;
use App\Model\Usuario;
use App\Model\Cliente;
use App\Model\Reserva;

class LoginController
{
    private $email;
    private $celular;
    private $senha;
    private $idCliente;
    private $variavel;

    public function index()
    {
        require APP . 'view/login/index.php';
    }
    public function acessoCliente()
    {
        $this->email = $_POST['email'];
        $this->senha = $_POST['senha'];
        $array = array();

        $cliente = new Cliente();
        $cliente = $cliente->acessoCliente($this->email);

        if (!empty($cliente)) {
            if (password_verify($this->senha, $cliente[0]->senha)) {
                $_SESSION['idCliente'] = $cliente[0]->id;
                $_SESSION['nomeCliente'] = $cliente[0]->nome_cliente;
                $_SESSION['imgCliente'] = $cliente[0]->img;
                $array += ["boleano" => 1];
                $array += ["msg" => "Acessando..."];
                echo json_encode($array);
            } else {
                $array += ["boleano" => 0];
                $array += ["msg" => "Senha incorreta"];
                echo json_encode($array);
            }
        } else {
            $array += ["boleano" => 0];
            $array += ["msg" => "Email não cadastrado"];
            echo json_encode($array);
        }
    }

    public function recuperarSenhaCliente()
    {
        $this->email = $_POST['email'];

        $cliente = new Cliente();
        $verificarCliente = $cliente->recuperarSenha($this->email);

        if (!empty($verificarCliente)) {

            $assunto 	= "Recuperar Senha";
            $nome 		= $verificarCliente[0]->nome_cliente;

            //Gerar Código  
            $gerarCodigo = substr(str_shuffle('Az23456789'), 1, 10);
            $novoCodigo = $verificarCliente[0]->id . 'X' . $gerarCodigo;

            $cliente = $cliente->recuperar($verificarCliente[0]->id, $novoCodigo);

            $emailClass = new EmailController("$nome","","$assunto",$this->email,URL.'login/verificaremailcliente/'.$novoCodigo,'templateRecuperarSenha()');

            $boleano = $emailClass->recuperarSenha();
            
            echo json_encode($boleano);
        } else {
            echo json_encode(0);
        }
    }

    public function verificarEmailCliente($codigo)
    {
        $verificar = explode("X", $codigo);

        $cliente = new Cliente();
        $cliente = $cliente->idRecuperar($verificar[0]);

        if ($cliente[0]->recuperar == $codigo) {
            header('location: ' . URL . 'login/novasenhacliente/' . $cliente[0]->id);
        } else {
            echo "Erro";
        }
    }

    public function novaSenhaCliente($idCliente)
    {
        $cliente = new Cliente();
        $clientes = $cliente->idNomeImg($idCliente);

        if (!empty($clientes[0]->recuperar)) {

            require APP . 'view/login/recuperar/head.php';
            require APP . 'view/login/recuperar/nova-senha-cliente.php';
            require APP . 'view/templates/footer-cliente.php';
        } else {
            header('location: ' . URL);
        }
    }

    public function salvarSenhaCliente()
    {
        $this->senha = $_POST['nova-senha'];
        $this->idCliente = $_POST['id'];

        $senhaHasg = password_hash($this->senha, PASSWORD_DEFAULT);

        $cliente = new Cliente();
        $atualizadoSenha = $cliente->atualizarSenha($this->idCliente, $senhaHasg);
        $limpadoRecuperar = $cliente->limparaRecuperar($this->idCliente);

        if ($atualizadoSenha && $limpadoRecuperar) {
            $_SESSION['idCliente'] = $this->idCliente;
            echo json_encode(1);
        } else {
            echo json_encode(0);
        }
    }

    public function carregarSession()
    {
        require APP . 'view/login/carregamento.php';
    }

    public function acesso()
    {
        // Recaptch
        if (isset($_POST['submit'])) {

            $secret = "6LfIVMYUAAAAAEekbFGWGEE5PDjXyZU01WoFq1Hf";
            $response = $_POST['g-recaptcha-response'];
            $verify = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret}&response={$response}");
            $captcha_success = json_decode($verify);

            if ($captcha_success->success == false) {
                $msgErro = 'reCaptcha não selecionado';
                $this->variavel = $msgErro;
                $this->index();
            } else if ($captcha_success->success == true) {
                $nomeEmail = $_POST['nome_email'];
                $senha = $_POST['senha'];

                $usuario = new Usuario();
                $usuario = $usuario->usuario($nomeEmail);

                if (!empty($usuario)) {
                    if (password_verify($senha, $usuario[0]->usuarioSenha)) {
                        $_SESSION['idUsuario'] = $usuario[0]->id;
                        $_SESSION['nomeUsuario'] = $usuario[0]->usuarioNome;
                        $_SESSION['nivelUsuario'] = $usuario[0]->usuarioNivel;
                        if ($usuario[0]->nivel == 'admin') {
                            header('location: ' . URL . 'admin');
                        } else if($usuario[0]->nivel == 'cabeleireiro')  {
                            header('location: ' . URL . 'profissional');
                        }
                        
                    } else {
                        $msgErro = 'Senha incorreta';
                        $this->variavel = $msgErro;
                        $this->index();
                    }
                } else {
                    $msgErro = 'usuário ou Email incorreto';
                    $this->variavel = $msgErro;
                    $this->index();
                }
            }
        }
    }

    public function recuperarSenha()
    {

        if (isset($_POST["recuperar"])) {

            $nomeEmail = $_POST['nome_email'];
            $usuario = new Usuario();
            $verificarUsuario = $usuario->usuario($nomeEmail);

            if (!empty($verificarUsuario)) {

                //Gerar Código  
                $gerarCodigo = substr(str_shuffle('Az23456789'), 1, 10);
                $novoCodigo = $verificarUsuario[0]->id . 'X' . $gerarCodigo;

                $usuario = $usuario->recuperar($verificarUsuario[0]->id, $novoCodigo);

            } else {
                $msgErro = 'Não existe usuário ou Email em nossos registro';
                $this->recuperarSenha();
            }
        }

        require APP . 'view/login/recuperar/head.php';
        require APP . 'view/login/recuperar/index.php';
    }

    public function verificarEmail($codigo)
    {
        $verificar = explode("X", $codigo);

        $usuario = new Usuario();
        $usuario = $usuario->usuario($verificar[0]);

        if ($usuario[0]->recuperar == $codigo) {
            header('location: ' . URL . 'login/novasenha/' . $usuario->id);
        } else {
            echo "Erro";
        }
    }

    public function novasenha($idUsuario)
    {

        $_SESSION['idUsuario'] = $idUsuario;

        $this->usuarioLongado();

        require APP . 'view/login/recuperar/head.php';
        require APP . 'view/login/recuperar/nova-senha.php';
        require APP . 'view/templates/footer.php';
    }

    public function analiseReserva()
    {
        date_default_timezone_set('America/Sao_paulo');
        $Data = date_create();
        $DataAtual = date_format($Data,"Y-m-d");

        /* validar os campos servico por ordem
        fazer o login  e mostrar que reserva foi feita com sucesso
        OU fazer o cadastro e mostrar que reserva com sucesso */

        //Reserva validacao
        $servico = serialize(json_decode($_POST['servico']));

        //LOGIN
        $this->email = $_POST['email'];
        $this->senha = $_POST['senha'];
        $array = array();

        $cliente = new Cliente();
        $cliente = $cliente->acessoCliente($this->email);

        if (!empty($cliente)) {
            if (password_verify($this->senha, $cliente[0]->senha)) {
                $_SESSION['idCliente'] = $cliente[0]->id;
                $_SESSION['nomeCliente'] = $cliente[0]->nome_cliente;
                $_SESSION['imgCliente'] = $cliente[0]->img;
                
                $reserva = new Reserva();
                $msgModal = $reserva->inserirCliente($cliente[0]->id,$servico,$_POST['observacao'], $_POST['profissional'], $DataAtual, "Aguardando");
                if ($msgModal) {
                    $array += ["boleano" => 1];
                    $array += ["msg" => "Reservado com sucesso."];
                }else{
                    $array += ["boleano" => 0];
                    $array += ["msg" => "Errol ao reservar, tente novamente"];
                }
                echo json_encode($array);
            } else {
                $array += ["boleano" => 0];
                $array += ["msg" => "Senha incorreta"];
                echo json_encode($array);
            }
        } else {
            $array += ["boleano" => 0];
            $array += ["msg" => "Email não cadastrado"];
            echo json_encode($array);
        }

        //cadastro
        //$_POST['nome'];
        //$_POST['email_cadastro'];
        //$_POST['celular'];

    }

    public function usuarioLongado()
    {
        if (!isset($_SESSION['idUsuario'])) {
            header('location: ' . URL . 'login');
        }
    }

    public function clienteLongado()
    {
        if (!isset($_SESSION['idCliente'])) {
            header('location: ' . URL);
        }
    }

    public function sairUsuario()
    {
        session_destroy();
        header('location: ' . URL . 'login');
    }

    public function sairCliente()
    {
        session_destroy();
        header('location: ' . URL);
    }


}
