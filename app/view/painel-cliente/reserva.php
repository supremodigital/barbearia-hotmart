<div class="content-wrapper container-sem-menu">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal modal-sem-menu d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Reserva feita com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal modal-sem-menu d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Erro ao reservar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title msg">Reserva</h4>
          <?php //foreach ($clienteLista as $linha) { ?>
          <form id="formularios">
            <div class="row">
              <div class="col-md-6 ">
                <div class="col-md-6 no-pad">
                  <div class="profissional-grupo">
                    <label>Escolha o profissional</label>
                    
                    <div class="form-group">
                      <img src="//localhost/weepy/dashboard-supremo/images/usuario/usuario.jpg" id="profissionalFoto" alt="image" >
                    </div>
                    <div class="form-group">
                      <select class="form-control" name="profissional" id="ProfissionalEscolha">
                      <option value="ambos" imagem="<?php echo URL . 'images/usuario/usuario.jpg'?>">Ambos</option>
                      <?php foreach ($profissionalLista as $profissional) { ?>
                        <option value="<?php echo $profissional->id;?>" imagem="<?php echo URL . 'images/usuario/'.$profissional->img;?>"><?php echo $profissional->nome;?></option>
                      <?php } ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Serviço</label>
                    <ul class="menu-servico">
                      <?php foreach ($categoriaLista as $linhaCategoria) { ?>
                      <?php foreach ($servicoLista as $key => $linha) { ?>
                      <?php if ($linhaCategoria->categoria == $linha->categoria) { ?>         
                        <li> 
                          <input type="checkbox" id="item<?php echo $key;?>">
                          <label for="item<?php echo $key;?>">
                            <div class="toggleIcon"></div><?php echo $linhaCategoria->categoria ?></label>
                          <div class="options">
                            <ul>
                              <?php foreach ($servicoLista as $linha) { ?>
                              <?php if ($linhaCategoria->categoria == $linha->categoria) { ?>
                              <li>
                                <div class="form-check form-check-success">
                                  <label class="form-check-label">
                                  <input type="checkbox" class="form-check-input" name="servico[]" value="<?php echo $linha->id ?>"><?php echo $linha->nome_servico . " - " . "R$" . $linha->valor; ?><i class="input-helper"></i></label>
                                </div>
                              </li>
                              <?php };?>
                              <?php };?>
                            </ul>
                          </div>
                        </li>
                      <?php break; };?>
                      <?php };?>
                      <?php };?>          
                    </ul>
                  </div>
                  <div class="form-group">
                    <label>Observação</label>
                    <textarea class="form-control" name="observacao" cols="30" rows="5"></textarea>
                  </div>
                  <div class="form-group">
                    <button type="button" id="reserva-chegada-cliente" class="btn btn-primary btn-sm">Entrar na fila</button>
                  </div>
              </div>
            </div>
          </form>
          <?php //} ?>
        </div>
      </div>
    </div>
  </div>
</div>