<?php

namespace App\Model;

use App\Core\Model;

class Cliente extends Model
{

    public function listaTodos()
    {
        $sql = "SELECT * FROM cliente";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaModulosCliente($id)
    {
        $sql = "SELECT cliente.*, modulo.* FROM inscricao INNER JOIN cliente ON cliente.idCliente = inscricao.idCliente INNER JOIN modulo ON modulo.idModulo = inscricao.idModulo WHERE cliente.idCliente = $id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function recuperarSenha($email)
    {
        $sql = "SELECT idCliente, cliNome FROM cliente WHERE cliEmail='$email'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function lista($id)
    {
        $sql = "SELECT * FROM cliente WHERE idCliente = $id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }


    public function idRecuperar($id)
    {
        $sql = "SELECT idCliente, cliRecuperar FROM cliente WHERE idCliente = $id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function recuperar($id, $recuperar)
    {
        $sql = "UPDATE cliente SET cliRecuperar = '$recuperar' WHERE idCliente = $id";
        $query = $this->db->prepare($sql);   
        $query->execute(); 
    }

    public function atualizarSenha($id, $senha)
    {
        $sql = "UPDATE cliente SET cliSenha = '$senha' WHERE idCliente = $id";
        $query = $this->db->prepare($sql);   
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

    public function idNomeImg($id)
    {
        $sql = "SELECT idCliente, cliNome, cliFoto, cliRecuperar FROM cliente WHERE idCliente = $id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
    
    public function limparaRecuperar($id)
    {
        $sql = "UPDATE cliente SET cliRecuperar = '' WHERE idCliente = $id";
        $query = $this->db->prepare($sql);  
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }
    

    public function acessocliente($email)
    {
        $sql = "SELECT idCliente, cliNome, cliSenha, cliFoto FROM cliente WHERE cliEmail = '$email'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
    
    public function atualizar(	$idCliente, 
								$cliNome, 
								$cliProfissao, 
								$cliEmail, 
								$cliSenha, 
								$cliRecuperar, 
								$cliFoto, 
								$cliFoneUm, 
								$cliFoneDois, 
								$cliStatus)
    {
        $sql = "UPDATE cliente set 	idCliente 		= '".$idCliente."' ,
									cliNome 		= '".$cliNome."' ,
									cliProfissao 	= '".$cliProfissao."' ,
									cliEmail 		= '".$cliEmail."' ,
									cliSenha 		= '".$cliSenha."' ,
									cliRecuperar 	= '".$cliRecuperar."' ,
									cliFoto 		= '".$cliFoto."' ,
									cliFoneUm 		= '".$cliFoneUm."' ,
									cliFoneDois 	= '".$cliFoneDois."' ,
									cliStatus 		= '".$cliStatus."' where idCliente = ".$id;
        $query = $this->db->prepare($sql);  
        if($query->execute()){
            return true;
        }else{
            return false;
        } 
    }

    public function inserir(	$cliNome, 
								$cliProfissao, 
								$cliEmail, 
								$cliSenha, 
								$cliRecuperar, 
								$cliFoto, 
								$cliFoneUm, 
								$cliFoneDois, 
								$cliStatus)
    {
        $sql = "INSERT INTO cliente (	cliNome, 
										cliProfissao, 
										cliEmail, 
										cliSenha, 
										cliRecuperar, 
										cliFoto, 
										cliFoneUm, 
										cliFoneDois, 
										cliStatus) VALUES (	:idCliente, 
															:cliNome, 
															:cliProfissao, 
															:cliEmail, 
															:cliSenha, 
															:cliRecuperar, 
															:cliFoto, 
															:cliFoneUm, 
															:cliFoneDois, 
															:cliStatus)";
        $query = $this->db->prepare($sql);
        $parameters = array(':idCliente'		=> $idCliente, 
							':cliNome' 			=> $cliNome, 
							':cliProfissao' 	=> $cliProfissao, 
							':cliEmail'	 		=> $cliEmail, 
							':cliSenha' 		=> $cliSenha, 
							':cliRecuperar' 	=> $cliRecuperar, 
							':cliFoto' 			=> $cliFoto, 
							':cliFoneUm' 		=> $cliFoneUm, 
							':cliFoneDois' 		=> $cliFoneDois, 
							':cliStatus' 		=> $cliStatus);
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

   
    public function existeEmail($cliEmail)
    {
        $sql = "SELECT idCliente FROM cliente WHERE cliEmail = '$cliEmail'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }


    public function desativar($id)
    {
        $sql = "UPDATE cliente SET cliStatus = '0' WHERE idCliente = $id";
        $query = $this->db->prepare($sql);
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

}
