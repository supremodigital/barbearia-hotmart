<?php

namespace App\Controller;

use App\Model\Nivel;

class NivelController
{
    public function nivelAcesso($controller, $action)
    {
        $nivelUsuario = $_SESSION['nivelUsuario'];

        // Pegar o nome do controler
        $nomeController = explode('\\', $controller);

        // verificar se nivel permite
        $nivel = new Nivel();
        $nivel = $nivel->acessoControler($nomeController[2] , $nivelUsuario);
        $acessoNegado = $nivel[0]->$nivelUsuario;
        if(!$acessoNegado){
            header('location: ' . URL . 'semacesso');
        }
    
    }

    public function controllerURL()
    {

    }
}
