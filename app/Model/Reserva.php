<?php

namespace App\Model;

use App\Core\Model;

class Reserva extends Model
{

    public function listaTodos()
    {
        $sql = "SELECT * FROM `reserva` WHERE 1 ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

	public function ServicosFinalizados()
    {
        $sql = "SELECT servico FROM reserva WHERE estatus='Finalizado'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaAtendimento($DataAtual, $id)
    {
        $sql = "SELECT reserva.id, reserva.servico, reserva.estatus, reserva.observacao, usuario.nome, cliente.nome_cliente FROM `reserva` INNER JOIN usuario ON reserva.id_usuario=usuario.id INNER JOIN cliente ON reserva.id_cliente=cliente.id WHERE data_servico='$DataAtual' && (estatus='Aguardando' OR estatus='Atendimento') && reserva.id_usuario=$id  ORDER BY `id` ASC";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function listaAtendimentoProfissional($DataAtual, $id)
    {
        $sql = "SELECT reserva.id, reserva.servico, reserva.estatus, reserva.observacao, usuario.nome, cliente.nome_cliente FROM `reserva` INNER JOIN usuario ON reserva.id_usuario=usuario.id INNER JOIN cliente ON reserva.id_cliente=cliente.id WHERE id_usuario='$id' && data_servico='$DataAtual' && (estatus='Aguardando' OR estatus='Atendimento') ORDER BY `id` ASC";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }
    
    public function qtdFila($id, $data)
    {
        $sql = "SELECT count(id) as fila from reserva where estatus = 'Aguardando' && id_usuario = $id && data_servico='$data'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function carregarAtendimento($DataAtual)
    {
        $sql = "SELECT reserva.id, reserva.servico, reserva.estatus, reserva.observacao, usuario.nome, cliente.nome_cliente FROM `reserva` INNER JOIN usuario ON reserva.id_usuario=usuario.id INNER JOIN cliente ON reserva.id_cliente=cliente.id WHERE data_servico='$DataAtual' && (estatus='Aguardando' OR estatus='Atendimento') ORDER BY `id` ASC ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function filaEspera($DataAtual)
    {
        $sql = "SELECT reserva.id, reserva.estatus, usuario.nome, cliente.nome_cliente, cliente.img, cliente.tipo_cadastro FROM `reserva` INNER JOIN usuario ON reserva.id_usuario=usuario.id INNER JOIN cliente ON reserva.id_cliente=cliente.id WHERE data_servico='$DataAtual' && (estatus='Aguardando' OR estatus='Atendimento') ORDER BY `id` ASC ";
        $query = $this->db->prepare($sql);
        $query->execute(); 

        return $query->fetchAll();
    }

    public function filaEsperaa($DataAtual)
    {
        $sql = "SELECT reserva.id, reserva.estatus, usuario.nome,usuario.img, cliente.nome_cliente, cliente.tipo_cadastro FROM `reserva` INNER JOIN usuario ON reserva.id_usuario=usuario.id INNER JOIN cliente ON reserva.id_cliente=cliente.id WHERE reserva.data_servico='$DataAtual' && (reserva.estatus='Aguardando' OR reserva.estatus='Atendimento') ORDER BY `id` ASC ";
        $query = $this->db->prepare($sql);
        $query->execute(); 

        return $query->fetchAll();
    }

    public function listaid_usuario($data)
    {
        $sql = "SELECT * FROM `reserva` WHERE data_servico='$data' AND (estatus='id_usuario' OR estatus='Aguardando') ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function lista($id)
    {
        $sql = "SELECT * FROM `reserva` WHERE id='$id'";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function verificar($id)
    {
        $sql = "SELECT id FROM `reserva` WHERE id_cliente='$id' && (estatus='Aguardando' OR estatus='Atendimento') ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function verificarReserva($id, $data)
    {
        $sql = "SELECT reserva.estatus, usuario.nome FROM `reserva` INNER JOIN usuario ON usuario.id=reserva.id_usuario WHERE reserva.data_servico='$data' && reserva.id_cliente=$id";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function posiçãoReserva($data)
    {
        $sql = "SELECT id_cliente FROM `reserva` WHERE data_servico='$data' && (estatus='Aguardando' OR estatus='Atendimento') ";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function atualizar($id, $nome, $servico, $observacao, $inicioid_usuario )
    {
        $sql = "update reserva set nome = '".$nome."', servico = '".$servico."',observacao = '".$observacao."',atendente = '".$inicioid_usuario."' where id = ".$id;
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    
    }

    public function id_usuario($data)
    {
        $sql = "SELECT * FROM `reserva` WHERE data_servico='$data' AND (estatus='Aguardando' OR estatus='id_usuario')";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $query->fetchAll();
    }

    public function atualizarStatus($id, $status, $inicio_atendimento )
    {
        $sql = "update reserva set estatus = '".$status."', inicio_atendimento = '".$inicio_atendimento."' where id = ".$id;
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    
    }

    public function cancelarReserva($id, $status)
    {
        $sql = "update reserva set estatus = '".$status."' where id_cliente='$id' ";
        $query = $this->db->prepare($sql);    

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    
    }

    public function inserir($nome,$servico,$observacao)
    {
        $sql = "INSERT INTO reserva (nome, servico, observacao, atendente) VALUES (:nome, :servico,  :observacao, :atendente)";
        $query = $this->db->prepare($sql);
        $parameters = array(':nome' => $nome, ':servico' => $servico, ':observacao' => $observacao);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function inserirCliente($id_cliente,$servico,$observacao, $id_usuario, $data, $estatus)
    {
        $sql = "INSERT INTO reserva (id_cliente, servico, observacao, id_usuario, data_servico, estatus) VALUES (:id_cliente, :servico,  :observacao, :id_usuario, :data_servico, :estatus)";
        $query = $this->db->prepare($sql);
        $parameters = array(':id_cliente' => $id_cliente, ':servico' => $servico, ':observacao' => $observacao, 'id_usuario' => $id_usuario, 'data_servico' => $data, 'estatus' => $estatus);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function inserirTeste($id_cliente,$servico,$data, $obs, $id_usuario, $estatus)
    {
        $sql = "INSERT INTO reserva (id_cliente, servico, data_servico, observacao, id_usuario, estatus) VALUES (:id_cliente, :servico, :data_servico, :observacao, :id_usuario, :estatus)";
        $query = $this->db->prepare($sql);
        $parameters = array(':id_cliente' => $id_cliente, ':servico' => $servico, ':data_servico' => "$data", ':observacao' => $obs, ':id_usuario' => $id_usuario, ':estatus' => $estatus);

        //Retonar SQL com sucesso ou erro
        if($query->execute($parameters)){
            return true;
        }else{
            return false;
        }
    }

    public function deletar($id)
    {
        $sql = "DELETE FROM reserva WHERE id = $id";
        $query = $this->db->prepare($sql);

        //Retonar SQL com sucesso ou erro
        if($query->execute()){
            return true;
        }else{
            return false;
        }
    }

}
