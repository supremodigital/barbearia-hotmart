<?php

namespace App\Controller;

use App\Controller\LoginController;
use App\Controller\NivelController;

class PainelController
{

    public function __construct()
    {
        (new LoginController)->usuarioLongado();
                        
        $nivelAcesso = new NivelController();
        $nivelAcesso = $nivelAcesso->nivelAcesso(get_class($this),__FUNCTION__);
    }
    
    public function index()
    {

        require APP . 'view/admin/head.php';
        require APP . 'view/templates/header.php';
        require APP . 'view/admin/index.php';
        require APP . 'view/templates/footer.php';
    }
}
