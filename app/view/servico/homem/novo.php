<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Salvo com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Erro ao salvar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM  -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title msg">Novo serviço para homens</h4>
          <span class="ajuda" data-tip="texto." tabindex="1"><i class="mdi mdi-help "></i></span>
          <form id="formularios">
            <div class="row">
              <div class="col-md-6 ">
                <div class="form-group">
                  <label for="exampleInputName1">Nome do serviço</label>
                  <input type="text" name="titulo" class="form-control" id="exampleInputName1">
                </div>
                <div class="form-group">
                  <label>Descrição</label>
                  <textarea name="descricao" class="form-control" rows="4"></textarea>
                </div>
                <div class="form-group">
                    <label>Categoria</label>
                    <span class="ajuda" data-tip="Tempo de duração do serviço em média" tabindex="1"><i class="mdi mdi-help "></i></span>
                    <select class="form-control" name="categoria">
                    <?php foreach ($categoriaLista as $linha) { ?>
                      <option value="<?php echo $linha->id; ?>"><?php echo $linha->categoria; ?></option>
                    <?php };?>
                    </select>
                  </div>
                <div class="form-group">
                  <label for="exampleInputName1">Valor</label>
                  <span class="ajuda" data-tip="Tempo de duração do serviço em média" tabindex="1"><i class="mdi mdi-help "></i></span>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">R$</span>
                        </div>
                        <input type="text" name="valor" class="form-control" placeholder="0.00">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="exampleInputName1">Duração do serviço</label>
                  <span class="ajuda" data-tip="Tempo de duração do serviço em média" tabindex="1"><i class="mdi mdi-help "></i></span>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="mdi mdi-av-timer icon-sm"></i></span>
                        </div>
                        <input type="time" name="tempo" class="form-control">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 ">
                <div class="form-group">
                  <label>Imagem</label>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="profile"
                        style="background-image: url('<?php echo URL; ?>images/img-upload-homem.jpg');">
                        <label class="edit">
                          <span><i class="mdi mdi-upload"></i></span>
                          <input type="file" size="32" name="imagem" id="inputImagem" value="">
                        </label>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label>Descrição da imagem</label>
                      <textarea name="alt" class="form-control" rows="4"></textarea>
                      <button type="button" id="editarImagem" class="btn btn-outline-primary btn-sm">Adicionar</button>
                      <button type="button" id="removerImagem" class="btn btn-outline-danger btn-sm">Excluir</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-3 grid-margin">
      <div class="card menu-atualizar">
        <div class="card-body">
          <h4 class="card-title">Menu serviço</h4>
          <div class="media">
            <i class="mdi mdi-backup-restore icon-md d-flex align-self-start mr-3"></i>
            <div class="media-body d-flex align-self-center">
              <p class="card-text">Atualizado </p>
            </div>
          </div>
          <div class="media">
            <span class="d-flex align-self-start mr-3">
              <img src="<?php echo URL;?>images/faces/face4.jpg" alt="profile"
                style="border-radius: 100%;border: 2px solid #ececec;width: 30px;">
            </span>
            <div class="media-body d-flex align-self-center">
              <p class="card-text">Editado por </p>
            </div>
          </div>
          <button destino="servicohomem/inserir" class="novo-ajax btn btn-inverse-success btn-rounded btn-fw">
            Salvar
          </button>
        </div>
      </div>
    </div>
  </div>
</div>

