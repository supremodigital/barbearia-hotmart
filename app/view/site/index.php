<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=0, user-scalable=no" />
<meta name="format-detection" content="telephone=no">
<title>Grupo Morbeck</title>
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="Cursos profissionalizantes de Barbearia.">
<meta name="author" content="Supremo Digital">
<link rel="shortcut icon" href="site/images/favicon.ico" type="image/x-icon" />

<!-- Necessary CSS Files -->
<link href="site/css/bootstrap.min.css" rel="stylesheet">
<!-- Bootstrap CSS Only GRID -->
<link rel="stylesheet" href="site/css/smoothslides.theme.css" type="text/css" />
<!--Main slider css-->
<link rel="stylesheet" href="site/css/slick.css" type="text/css" />
<!--Service slider css-->
<link rel="stylesheet" href="site/css/jquery.fancybox.css" type="text/css" />
<!--fancybox css-->
<link rel="stylesheet" href="site/css/styles.css" type="text/css" />
<!--custom css-->
<link rel="stylesheet" href="site/css/supremo.css" type="text/css" />
<!--custom css-->
<link rel="stylesheet" href="site/css/fonts/icofont.html" type="text/css" />
<!--Icofont css-->
<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">
<!--google font oswald-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<!--google font Open Sans--> 

<!--[if IE]>
    	<script type="text/javascript" src="site/js/html5shiv.js"></script>
    <![endif]-->

    <script type="text/javascript">
	function importHotmart(){ 
 		var imported = document.createElement('script'); 
 		imported.src = 'https://static.hotmart.com/checkout/widget.min.js'; 
 		document.head.appendChild(imported); 
		var link = document.createElement('link'); 
		link.rel = 'stylesheet'; 
		link.type = 'text/css'; 
		link.href = 'https://static.hotmart.com/css/hotmart-fb.min.css'; 
		document.head.appendChild(link);	} 
 	importHotmart(); 
 </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '2712680778999601');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=2712680778999601&ev=PageView&noscript=1"/>
    </noscript>
<!-- End Facebook Pixel Code -->
</head>

<body>
<div class="se-pre-con"></div>
<!--<div class="containerr">
    <div class="pole">
      <div class="top"></div>
      <div class="cilinder"></div>
      <div class="bottom"></div>
    </div>
  </div>-->

<!--banner -->
<section id="home" class="banner-slider-initial"> 
    <!--slider-->
    <div class="banner-slide-show-wrapper">
        <div class="banner-slider" id="banner-slide-show">
            <div class="banner-slider-item">
                <div class="banner-slider-item-img" style="background-image: url(site/images/banner-01.jpg);">
                    <div class="banner-text"> <span>prêmio</span>
                        <h1>MORBECK</h1>
                        <a onclick="return false;"  class="hotmart-fb hotmart__button-checkout read-more-btn" href="https://pay.hotmart.com/N43098343N?checkoutMode=2" >Nosso Curso</a> </div>
                </div>
            </div>
            <div class="banner-slider-item">
                <div class="banner-slider-item-img" style="background-image: url(site/images/banner-02.jpg);">
                    <div class="banner-text"> <span>prêmio</span>
                        <h2>TESOURA DE OURO NO PROGRAMA DUELO DE SALÕES</h2>

                        <a onclick="return false;"  class="hotmart-fb hotmart__button-checkout read-more-btn" href="https://pay.hotmart.com/N43098343N?checkoutMode=2" >Seja Profissional Agora</a> </div>
                </div>
            </div>
            <div class="banner-slider-item">
                <div class="banner-slider-item-img" style="background-image: url(site/images/banner-01.jpg);">
                    <div class="banner-text"> <span>prêmio</span>
                        <h2>DESTAQUE EM CABELO AFRO</h2>
                        <a onclick="return false;"  class="hotmart-fb hotmart__button-checkout read-more-btn" href="https://pay.hotmart.com/N43098343N?checkoutMode=2" >Se Destaque</a> </div>
                </div>
            </div>
            <div class="banner-slider-item">
                <div class="banner-slider-item-img" style="background-image: url(site/images/banner-03.jpg);">
                    <div class="banner-text"> <span>prêmio</span>
                        <h2>PREMIO DESTAQUE ENTRE BARBEIROS DO BRASIL</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Navigation -->
    <div class="slider-header">
        <div class="container">
            <div class="row"> 
                <!--Navigation - Center Logo header-->
                <div class="header">
                    <div class="mobile-ic"> <span></span> <span></span> <span></span> </div>
                    <div class="responsive-menu">
                        <ul>
                            <li class="active"><a href="#home" title="Home">Início</a></li>
                            <li><a href="#about" title="Sobre nós">Sobre Nós</a></li>
                            <li><a href="#depoimento" title="Depoimentos">Depoimentos</a></li>
                            <li><a href="#our-team" title="Equipe">Equipe</a></li>
                            <li><a href="#curso" title="Pricing">Cursos</a></li>
                            <li><a href="#location" title="Location">Contato</a></li>
                            <li><a onclick="return false;"  class="hotmart-fb hotmart__button-checkout" href="https://pay.hotmart.com/N43098343N?checkoutMode=2">Torna-se Profissional</a></li>
                        </ul>
                    </div>
                    <div class="table-cell">
                        <ul>
                            <li class="active"><a href="#home" title="Inicio">Início</a></li>
                            <li><a href="#about" title="Sobre nós">Sobre Nós</a></li>
                            <li><a href="#depoimento" title="Depoimentos">Depoimentos</a></li>
                        </ul>
                    </div>
                    <div class="logo"><a href="#home" title="Barber Shop"><img src="site/images/logo/gm-logo.png" alt="logo"></a> </div>
                    <div class="table-cell">
                        <ul>
                            <li><a href="#our-team" title="Equipe">Equipe</a></li>
                            <li><a href="#curso" title="Pricing">Cursos</a></li>
                            <li><a href="#location" title="Location">Contato</a></li>
                            <li><a onclick="return false;"  class="hotmart-fb hotmart__button-checkout" href="https://pay.hotmart.com/N43098343N?checkoutMode=2">Torna-se Profissional</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<!-- Sobre -->
<section id="about" class="experience-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="about-left-section">
                    <h6>GRUPO MORBECK - HISTÓRIA</h6>
                    <h2>5 anos ensino com qualidade</h2>
                    <span class="line"></span>
                    <div class="story-text">
                        <div class="story-text-left">
                            <p class="big-text">A Barbearia Morbeck, fundada em 2011, está no mercado de Barbearia há quase 10 anos, com a primeira unidade situada em um bairro chamado Itaim Paulista na zona leste de São Paulo SP Brasil.</p>
                            <p>Hoje a Empresa Morbeck tem adquirido no mercado algumas certificações e reconhecimento através de alguns <strong>prêmios como:</strong></p>
                            <p class="big-text supremo-text">DESTAQUE EM CABELO AFRO</p>
                            <p class="big-text supremo-text">TESOURA DE OURO NO PROGRAMA DUELO DE SALÕES</p>
                            <p class="big-text supremo-text">PREMIO DESTAQUE ENTRE BARBEIROS DO BRASIL.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="about-right-section"> <img src="site/images/about-image.png" alt="aboutimage"> </div>
            </div>
        </div>
    </div>
</section>

<!-- Apresentação -->
<section class="get-discount">
    <div class="container">
        <div class="row">
            <div class="get-discount-box supremo-video">
            <iframe width="100%" height="450" src="https://www.youtube-nocookie.com/embed/E11dxDLY7dY?controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section> 
    
<!-- CURSO -->
<section id="curso" class="experience-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="about-left-section"> <img src="site/images/about-image.png" alt="aboutimage"> </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="about-left-section">
                    <h2>CURSOS E TREINAMENTOS</h2>
                    <span class="line"></span>
                    <div class="story-text">
                        <div class="story-text-left supremo-story">
                            <p>Ao longo dos anos conquistou algumas unidades no Brasil, estrutura preparada para Curso e treinamentos para iniciantes, hoje com mais de 300 alunos formados e Atualmente expandindo para Nova Unidade na Europa Portugal Temos como principais valores o mais elevado grau de qualidade e a eficiência de uma equipe altamente qualificada, por isso nossos profissionais são constantemente treinados interna e externamente, além de serem motivados, atingindo excelentes níveis de produtividade e qualidade dos serviços, exercendo suas funções em um ambiente de trabalho saudável, ético, comprometido e onde possam ser oferecidas novas oportunidades de ascensão profissional e tratamento humanizado.</p>
                            <p>Direcionar os termos das negociações com clientes e fornecedores de maneira ética e profissional para criar uma relação profícua e duradoura, sempre com Produtos certificados, com a mais alta qualidade tecnológica.</p>
                            <ul>
                                <li>Princípio da equidade</li>
                                <li>Respeito interpessoal</li>
                                <li>Respeito com o Meio Ambiente</li>
                                <li>Responsabilidade Social</li>
                                <li>Estímulo ao desenvolvimento dos nossos colaboradores</li>
                                <li>Compromisso com Resultados</li>
                            </ul>
                        </div>
                    </div>
                    <!--<button title="Comprar Curso" class="popup-preinscricao read-more-btn" style="margin-top:15px">Faça sua pré inscrição</button> </div>-->
                    <a onclick="return false;"  class="hotmart-fb hotmart__button-checkout read-more-btn"href="https://pay.hotmart.com/N43098343N?checkoutMode=2" >Faça sua inscrição</a>
            </div>
        </div>
    </div>
</section>

<!-- Depoimento Alunos -->
<section id="depoimento" class="our-service">
    <div class="container">
        <div class="row">
            <h2 class="heading-title">Depoimentos Alunos</h2>
            <div class="service-wrapper clearfix">
                <div class="service-block service-middle">
                    <div class="service-block-inner">
                        <div class="service-block-image supremo-video">
                            <video controls preload="metadata" poster="site/images/apresentacao.png" src="site/video/video-morbeck-01.mp4"></video>                        
                        </div>
                    </div>
                </div>
                <div class="service-block service-middle">
                    <div class="service-block-inner">
                        <div class="service-block-image supremo-video">
                            <video controls src="site/video/video-morbeck-04.mp4"></video>
                        </div>
                    </div>
                </div>
                <div class="service-block service-middle">
                    <div class="service-block-inner supremo-video">
                        <video controls src="site/video/video-morbeck-03.mp4"></video>
                    </div>
                </div>
                <div class="service-block service-middle">
                    <div class="service-block-inner supremo-video">
                        <video controls src="site/video/video-morbeck-02.mp4"></video>
                    </div>
                </div>
                <div class="service-block service-middle">
                    <div class="service-block-inner supremo-video">
                        <video controls src="site/video/video-morbeck-05.mp4"></video>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Equipe -->
<section id="our-team" class="client-testimonial">
    <div class="container">
        <div class="row">
            <h2 class="heading-title">Equipe</h2>
            <div class="our-team-wrapper clearfix">
                <div class="our-team-box">
                    <div class="our-team-inner">
                        <div class="our-team-img"> <img src="site/images/equipe/WILLIAM.jpg" alt=">WILLIAM MENDES"> </div>
                        <div class="our-team-text">
                            <h5>WILLIAM MENDES</h5>
                            <span>Diretor Executivo</span> </div>
                    </div>
                </div>
                <div class="our-team-box">
                    <div class="our-team-inner">
                        <div class="our-team-img"> <img src="site/images/equipe/WELLYSON.jpg" alt="WELLYSON HENRIQUE"> </div>
                        <div class="our-team-text">
                            <h5>WELLYSON HENRIQUE</h5>
                            <span>Diretor de Vendas</span> </div>
                    </div>
                </div>
                <div class="our-team-box">
                    <div class="our-team-inner">
                        <div class="our-team-img"> <img src="site/images/equipe/FELIPE.jpg" alt="FELIPE MORBECK"> </div>
                        <div class="our-team-text">
                            <h5>FELIPE MORBECK</h5>
                            <span>CEO</span> </div>
                    </div>
                </div>
                <div class="our-team-box">
                    <div class="our-team-inner">
                        <div class="our-team-img"> <img src="site/images/equipe/TIAGO.jpg" alt="TIAGO MELLO"> </div>
                        <div class="our-team-text">
                            <h5>TIAGO MELLO</h5>
                            <span>Diretor de Markentig</span> </div>
                    </div>
                </div>
                <div class="our-team-box">
                    <div class="our-team-inner">
                        <div class="our-team-img"> <img src="site/images/equipe/THIAGO.jpg" alt="THIAGO LUCENA"> </div>
                        <div class="our-team-text">
                            <h5>THIAGO LUCENA</h5>
                            <span>Diretor Financeiro</span> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

    
<!-- Certificado -->
<section class="get-discount">
    <div class="container">
        <div class="row">
            <div class="get-discount-box"> <img src="site/images/certificado/certificado.jpg" alt="certificado Morbeck"> </div>
            <p>Certificado Incluso
Válido em todo o território nacional!
"Os cursos livres tem como base legal o Decreto Presidencial N°5.154  de 23 de Julho de 2004"</p>
 
    <a onclick="return false;"  class="hotmart-fb hotmart__button-checkout read-more-btn"href="https://pay.hotmart.com/N43098343N?checkoutMode=2" >Adquira o Seu</a>
    <!--<button title="Make an appointment" class="popup-preinscricao read-more-btn">Adquira o Seu</button> </div>-->
    </div>
</section>

<!-- Contato -->
<section id="location" class="map-banner">
    <div class="container">
        <div class="row">
            <h2 class="heading-title">Entre em contato</h2>
            <div class="map-top-ic clearfix">
                <div class="map-top-block">
                    <div class="map-inner-ic"> <i class="icofont icofont-smart-phone"></i> </div>
                    <div class="map-top-text"> <span>Telefone</span>
                        <p><a href="https://api.whatsapp.com/message/O7UXJYH3YRUMN1" target="_blank" rel="noopener noreferrer"> 11 9 9013-4033</a></p>
                    </div>
                </div>
                <div class="map-top-block">
                    <div class="map-inner-ic"> <i class="icofont icofont-envelope-open"></i> </div>
                    <div class="map-top-text"> <span>Email</span>
                        <p><a href="mailto:youremailid@gmail.com" title="youremailid@gmail.com">contato@grupo-morbeck.com</a></p>
                    </div>
                </div>
                <div class="map-top-block">
                    <div class="map-inner-ic"> <i class="icofont icofont-clock-time"></i> </div>
                    <div class="map-top-text"> <span>Funcionamento</span>
                        <p>8:00 - 18:00</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Footer section -->
<section class="footer">
    <div class="container">
        <div class="footer-logo"> <img src="site/images/logo/gm-logo-com-texto.png" alt="logo Morbeck"> </div>
        <div class="row">
            <div class="footer-nav">
                <ul>
                    <li class="active"><a href="#home" title="Home">Início</a></li>
                    <li><a href="#about" title="Sobre nós">Sobre Nós</a></li>
                    <li><a href="#depoimento" title="Depoimentos">Depoimentos</a></li>
                    <li><a href="#our-team" title="Equipe">Equipe</a></li>
                    <li><a href="#curso" title="Pricing">Cursos</a></li>
                    <li><a href="#location" title="Location">Contato</a></li>
                </ul>
            </div>
            <div class="subscribe-us clearfix">
                <div class="social-ic">
                    <ul>
                        <li><a href="https://www.facebook.com/GRUPOMORBECKS" title="Facebook" target="_black"><i class="icofont icofont-social-facebook"></i></a></li>
                        <li><a href="https://instagram.com/grupomorbecks?igshid=15tw5f7vhmu8e" title="Twitter" target="_black"><i class="icofont icofont-social-instagram"></i></a></li>
                        <!--<li><a href="# " title="Google plus"><i class="icofont icofont-social-google-plus"></i></a></li>
                        <li><a href="# " title="Pintrest"><i class="icofont icofont-social-pinterest"></i></a></li>
                        <li><a href="# " title="youtube"><i class="icofont icofont-social-youtube-play"></i></a></li>-->
                    </ul>
                </div>
                <p class="copyright">&copy; 2020 GRUPO MORBECK <span><a href="https://supremodigital.com.br" target="_blank"> Supremo Digital </a></span></p>
            </div>
        </div>
        <a href="#top" class="scroll-top">TOPO</a> </div>
</section>
<!-- MODAL LOGIN -->
<div class="pop-up">
  <div class="content">
    <div class="container">
      <div class="dots">
        <div class="dot"></div>
        <div class="dot"></div>
        <div class="dot"></div>
      </div>
      
      
      <span class="close">Fechar</span>
      
      <div class="title">

        <h2>Pré inscrição</h2>
      </div>
      
      <img src="site/images/logo/gm-logo-com-texto.png" alt="logo Morbeck">
      
      <div class="subscribe">
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <form class="modal-1">
        <div class="messages"></div>
            <div class="row">
                <div class="col-md-6">
                    <input type="text" placeholder="Nome" name="nome" required="required" data-error="Campo Obrigatório." style="margin-bottom: 10px;">
                </div>
                <div class="col-md-6">
                    <input type="text" placeholder="Sobrenome" name="sobrenome" required="required" data-error="Campo Obrigatório." style="margin-bottom: 10px;">
                </div>
                <div class="col-md-12">
                    <input type="email" placeholder="Seu melhor e-mail" name="email" required="required" data-error="Campo Obrigatório." style="margin-bottom: 10px;">
                </div>
                <div class="col-md-12">
                    <input type="text" placeholder="(DDD) 9 9999-9999" name="celular" required="required"data-error="Campo Obrigatório." style="margin-bottom: 10px;">
                </div>
                <div class="col-md-12" style="margin-bottom: 10px;">
                    <div class="form-group">
                        <div class="g-recaptcha" data-sitekey="6Le-dAAVAAAAADGhUSXgYo7yq5CNKj1RorWM5wKJ"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <input type="submit" class="quote_btn theme_btn" value="Enviar">
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>

<!--<form class="modal-1">

            <div class="messages">
        
            </div>

            <div class="controls">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="form_name">Nome</label>
                            <input id="form_name" type="text" name="nome" class="form-control" placeholder="" required="required" data-error="Campo Obrigatório.">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="form_lastname">Celular</label>
                            <input id="form_lastname" type="text" name="phone" class="form-control" >
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="form_email">Email</label>
                            <input id="form_email" type="email" name="email" class="form-control" placeholder="" required="required" data-error="Campo Obrigatório.">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                            <label for="form_phone">CPF</label>
                        <div class="form-group">
                            <input id="form_phone" type="tel" name="cpf" class="form-control" placeholder="" required="required" data-error="Campo Obrigatório.">
                            <div class="help-block with-errors"></div>
                        </div>
					</div>
					<div class="col-md-6">
                            <label for="form_phone">Placa do carro</label>
                        <div class="form-group">
                            <input id="form_phone" type="text" name="placa" class="form-control" placeholder="" required="required" data-error="Campo Obrigatório.">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
					<div class="col-md-6">
						<label for="form_phone">Data de nascimento</label>
                        <div class="form-group">
                            <input id="form_phone" type="tel" name="nascimento" class="form-control" placeholder="00/00/0000" required="required" data-error="Campo Obrigatório.">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
					<div class="col-md-6">
						<label for="form_phone">Número do contrato</label>
                        <div class="form-group">
                            <input type="text" name="contrato" class="form-control">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="form_message">Sua Mensagem</label>
                            <textarea id="form_message" name="message" class="form-control" placeholder="" rows="4"></textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="g-recaptcha" data-sitekey="6Le-dAAVAAAAADGhUSXgYo7yq5CNKj1RorWM5wKJ"></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <input type="submit" class="quote_btn theme_btn" value="Enviar">
                    </div>
                </div>
            </div>
        </form>-->
<script type="text/javascript" src="site/js/jquery1.12.4.min.js"></script> 
<!--jquery Reference--> 
<script type="text/javascript" src="site/js/jquery.fancybox.js"></script> 
<!--fancybox jquery--> 
<script type="text/javascript" src="site/js/smoothslides-2.2.1.min.js"></script> 
<!--smooth slider js--> 
<script type="text/javascript" src="site/js/slick.min.js"></script> 
<!--Service slider jquery--> 
<script type="text/javascript" src="site/js/custom.js"></script> 
<!--custom jquery Reference--> 
<script type="text/javascript" src="site/js/tab.js"></script> 


<script src='site/php/bootstrap.min.js'></script>
<script src="site/php/validator.js"></script>
<script src="site/php/contact-2.js"></script>

</body>
</html>