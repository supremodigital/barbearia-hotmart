<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <title>Login - Morbeck</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css'>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css'>
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
    <link rel="stylesheet" href="teste/style.css">

</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-2"></div>
            <div class="col-lg-6 col-md-8 login-box">
                <div class="col-lg-12 login-key">
                    <img src="site/images/logo/gm-logo-com-texto.png" alt="">
                </div>
                <div class="col-lg-12 login-title">
                    PAINEL
                </div>

                <div class="col-lg-12 login-form">
                    <div class="col-lg-12 login-form">

                        <script src='https://www.google.com/recaptcha/api.js'></script>
                        <form class="pt-3" id="formLogin" method="POST" action="<?php echo URL; ?>login/acesso">
                            <div class="form-group">
                                <label class="form-control-label">E-mail</label>
                                <div class="input-group">
                                    <div class="input-group-prepend bg-transparent">
                                        <span class="input-group-text bg-transparent border-right-0">
                                            <i class="mdi mdi-account-outline text-primary"></i>
                                        </span>
                                    </div>
                                    <input type="text" name="nome_email" class="form-control form-control-lg border-left-0" id="exampleInputEmail" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label">Senha</label>
                                <div class="input-group">
                                    <div class="input-group-prepend bg-transparent">
                                        <span class="input-group-text bg-transparent border-right-0">
                                            <i class="mdi mdi-lock-outline text-primary"></i>
                                        </span>
                                    </div>
                                    <input type="password" name="senha" class="form-control input-senha form-control-lg border-left-0" id="exampleInputPassword" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text olhar-senha"><i class="mdi mdi-eye-off icon-md"></i></span>
                                    </div>
                                </div>
                            </div>
                            <?php if (isset($msgErro)) {
                                echo $msgErro;
                            }; ?>
                            <div class="my-3">
                                <div class="g-recaptcha" data-sitekey="6LfIVMYUAAAAAC2sUt_0SZ3SECr9I0v0WBQVAsU-"></div>
                            </div>
                            <div class="my-3">
                                <button type="submit" name="submit" class="btn btn-block btn-outline-primary">LOGIN</button>
                            </div>
                            <div class="my-2 d-flex justify-content-between align-items-center">
                                <div class="form-check">
                                    <label class="form-check-label text-muted">
                                        <input type="checkbox" class="form-check-input">
                                        Mantenha-me conectado
                                    </label>
                                </div>
                                <a href="<?php echo URL; ?>login/recuperarSenha" class="auth-link .text-branco">Esqueceu a senha?</a>
                            </div>
                         
                        </form>
                    </div>
                </div>
                <div class="col-lg-3 col-md-2"></div>
            </div>
        </div>
        <script src="<?php echo URL; ?>js/script.js"></script>
</body>

</html>