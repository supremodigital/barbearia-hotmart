<div class="content-wrapper">
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO-->
  <div class="row ativo-msg-modal d-none" id="atualizado-sucesso">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Atualizado com sucesso.</p>
          <div class="d-flex">
            <button id="bannerClose1" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row ativo-msg-modal d-none" id="erro-atualizar">
    <div class="col-md-9">
      <div class="card border-0">
        <div class="card-body py-3 px-4 d-flex align-items-center justify-content-between flex-wrap">
          <p class="mb-0 text-white font-weight-medium">Erro ao atualizar.</p>
          <div class="d-flex">
            <button id="bannerClose2" class="btn border-0 p-0">
              <i class="mdi mdi-close text-white"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- NOTIFICAÇÂO ATUALIZAÇÂO FIM   -->
  <div class="row">
    <div class="col-md-9 grid-margin">
      <div class="card grid-margin">
        <div class="card-body">
          <h4 class="card-title">Editar usuário</h4>
          <p class="card-description">
            Conteúdo de destaque do site com seu serviço ou produto.
          </p>
          <?php foreach ($usuarioLista as $linha) { ?>
          <form id="formularios">
            <div class="row">
            <div class="col-md-6 ">
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                    <div class="form-group">
                      <div class="profile"
                        style="background-image: url('<?php echo URL; ?>images/usuario/<?php if(property_exists($linha, 'usuarioImg')){echo $linha->usuarioImg;}else{echo'img-upload.png';};?>');">
                        <label class="edit">
                          <span><i class="mdi mdi-upload"></i></span>
                          <input type="file" size="32" name="imagem" id="inputImagem" value="">
                        </label>
                      </div>
                      </div>
                      <div class="form-group">
                        <button type="button" id="editarImagem" class="btn btn-outline-primary btn-sm">Escolher</button>
                        <button type="button" id="removerImagem" class="btn btn-outline-danger btn-sm">Remover</button>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Nome</label>
                        <input type="text" name="nome" class="form-control" placeholder="Name" value="<?php echo $linha->usuarioNome;?>">
                      </div>
                      <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" class="form-control" placeholder="Name" value="<?php echo $linha->usuarioEmail;?>">
                      </div>
                      <div class="form-group">
                        <label>Nível</label>
                        <select name="nivel" class="form-control">
                        <?php foreach ($nivelLista as  $linhaa) {?>
                          <option value="<?php echo $linhaa->id;?>"<?php if($linhaa->nivel == $linha->usuarioNivel){echo 'selected';}?>><?php echo $linhaa->nivel;?></option>
                        <?php } ?>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 ">
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Celular</label>
                        <input type="text" name="celular" class="form-control" placeholder="" value="<?php echo $linha->usuarioCelular;?>">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row" style="justify-content: flex-end;">
              <button destino="usuario/atualizar/<?php echo $linha->id;?>" class="link-ajax-atualizar btn btn-inverse-primary btn-rounded btn-fw">
                Atualizar
              </button>
            </div>
          </form>         
        </div>
      </div>    
    </div>
    <?php } ?>
  </div>
</div>